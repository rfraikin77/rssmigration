//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RssMigrator.OldContext
{
    using System;
    using System.Collections.Generic;
    
    public partial class ResourceCode
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ResourceCode()
        {
            this.ResourceTranslations = new HashSet<ResourceTranslation>();
        }
    
        public int ResourceCodeId { get; set; }
        public int ResourceGroupId { get; set; }
        public int ResourceTypeId { get; set; }
        public string Name { get; set; }
    
        public virtual ResourceGroup ResourceGroup { get; set; }
        public virtual ResourceType ResourceType { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ResourceTranslation> ResourceTranslations { get; set; }
    }
}
