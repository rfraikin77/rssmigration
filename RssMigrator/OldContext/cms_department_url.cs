//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RssMigrator.OldContext
{
    using System;
    using System.Collections.Generic;
    
    public partial class cms_department_url
    {
        public int departmentID { get; set; }
        public string URL { get; set; }
        public bool @default { get; set; }
    
        public virtual cms_department cms_department { get; set; }
    }
}
