//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RssMigrator.OldContext
{
    using System;
    using System.Collections.Generic;
    
    public partial class cmsx_tradeshow
    {
        public int revisionId { get; set; }
        public string name { get; set; }
        public string location { get; set; }
        public Nullable<System.DateTime> startdate { get; set; }
        public Nullable<System.DateTime> enddate { get; set; }
        public string admission { get; set; }
        public string stand { get; set; }
        public string info { get; set; }
        public string infoURL { get; set; }
        public string reportURL { get; set; }
    
        public virtual cms_revision cms_revision { get; set; }
    }
}
