//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RssMigrator.OldContext
{
    using System;
    using System.Collections.Generic;
    
    public partial class tvc_release_note_text
    {
        public int noteID { get; set; }
        public int localeID { get; set; }
        public string title { get; set; }
        public string SolutionDescription { get; set; }
        public System.DateTime dateCreated { get; set; }
        public string ProblemDescription { get; set; }
    
        public virtual Locale Locale { get; set; }
        public virtual tvc_release_note tvc_release_note { get; set; }
    }
}
