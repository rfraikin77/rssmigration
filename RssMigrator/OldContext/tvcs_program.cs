//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RssMigrator.OldContext
{
    using System;
    using System.Collections.Generic;
    
    public partial class tvcs_program
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tvcs_program()
        {
            this.ProgramFtpMappings = new HashSet<ProgramFtpMapping>();
            this.Releases = new HashSet<Release>();
            this.ter_error_report = new HashSet<ter_error_report>();
            this.tvc_release_note = new HashSet<tvc_release_note>();
            this.tvcs_menu = new HashSet<tvcs_menu>();
            this.tvcs_module = new HashSet<tvcs_module>();
            this.tvcs_program1 = new HashSet<tvcs_program>();
            this.t_customer = new HashSet<t_customer>();
            this.tvc_release_note1 = new HashSet<tvc_release_note>();
        }
    
        public int programID { get; set; }
        public string code { get; set; }
        public bool has_menus { get; set; }
        public bool has_modules { get; set; }
        public Nullable<int> parentID { get; set; }
        public Nullable<int> licenseModuleID { get; set; }
        public string exe { get; set; }
        public string exe_old { get; set; }
        public bool deleted { get; set; }
        public int order { get; set; }
        public bool IsTransitoVersion { get; set; }
        public int ProgramGroupId { get; set; }
        public int ReleaseStrategyId { get; set; }
        public bool InheritParentLicense { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProgramFtpMapping> ProgramFtpMappings { get; set; }
        public virtual ProgramGroup ProgramGroup { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Release> Releases { get; set; }
        public virtual ReleaseStrategy ReleaseStrategy { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ter_error_report> ter_error_report { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tvc_release_note> tvc_release_note { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tvcs_menu> tvcs_menu { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tvcs_module> tvcs_module { get; set; }
        public virtual tvcs_module tvcs_module1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tvcs_program> tvcs_program1 { get; set; }
        public virtual tvcs_program tvcs_program2 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<t_customer> t_customer { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tvc_release_note> tvc_release_note1 { get; set; }
    }
}
