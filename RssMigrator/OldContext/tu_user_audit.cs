//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RssMigrator.OldContext
{
    using System;
    using System.Collections.Generic;
    
    public partial class tu_user_audit
    {
        public int userID { get; set; }
        public Nullable<int> audit_userID { get; set; }
        public System.DateTime audit_date { get; set; }
        public int audit_action { get; set; }
    
        public virtual tu_user tu_user { get; set; }
        public virtual tu_user tu_user1 { get; set; }
    }
}
