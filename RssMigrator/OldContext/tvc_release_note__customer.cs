//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RssMigrator.OldContext
{
    using System;
    using System.Collections.Generic;
    
    public partial class tvc_release_note__customer
    {
        public int noteID { get; set; }
        public int customerID { get; set; }
        public System.DateTime dateCreated { get; set; }
        public bool addedByErrorReport { get; set; }
        public string ExternalReference { get; set; }
    
        public virtual t_customer t_customer { get; set; }
        public virtual tvc_release_note tvc_release_note { get; set; }
    }
}
