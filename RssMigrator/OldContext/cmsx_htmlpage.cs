//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RssMigrator.OldContext
{
    using System;
    using System.Collections.Generic;
    
    public partial class cmsx_htmlpage
    {
        public int revisionId { get; set; }
        public string title { get; set; }
        public string body { get; set; }
        public string css { get; set; }
        public string javascript { get; set; }
        public string meta_title { get; set; }
        public string meta_description { get; set; }
        public string meta_keywords { get; set; }
    
        public virtual cms_revision cms_revision { get; set; }
    }
}
