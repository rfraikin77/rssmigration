﻿using System;
using RssMigrator.MigrationSteps.Migrations;
using RssMigrator.NewContext;
using RssMigrator.OldContext;

namespace RssMigrator.MigrationSteps
{
    public class Migrator
    {
        // Init DB connection(s)
        public static OldEntities oldContext = new OldEntities();
        public static NewEntities newContext = new NewEntities();

        public static string TableMigrationJournal = string.Empty;

        public void Start()
        {
            Console.WriteLine("Migration has started..");
            Console.WriteLine("");
            Console.WriteLine($"[ 1 / 95] - CDATA => {new tbl_cdata().Transfer()}");
            Console.WriteLine($"[ 2 / 95] - CGLOBAL => {new tbl_cglobal().Transfer()}");
            Console.WriteLine($"[ 3 / 95] - CMS_DEPARTMENT => {new tbl_cms_department().Transfer()}");
            Console.WriteLine($"[ 4 / 95] - CMS_DEPARTMENT_URL => {new tbl_cms_department_url().Transfer()}");
            Console.WriteLine($"[ 5 / 95] - CMS_OBJECT => {new tbl_cms_object().Transfer()}");
            Console.WriteLine($"[ 6 / 95] - CMS_OBJECT_PATH => {new tbl_cms_object_path().Transfer()}");
            Console.WriteLine($"[ 7 / 95] - CMS_OBJECT_TYPE => {new tbl_cms_object_type().Transfer()}");
            Console.WriteLine($"[ 8 / 95] - CMS_PATH => {new tbl_cms_path().Transfer()}");
            Console.WriteLine($"[ 9 / 95] - CMS_REVISION => {new tbl_cms_revision().Transfer()}");
            Console.WriteLine($"[10 / 95] - CMS_REVISION_LIVE => {new tbl_cms_revision_live().Transfer()}");
            Console.WriteLine($"[11 / 95] - CMS_REVISION_LIVE_AUDIT => {new tbl_cms_revision_live_audit().Transfer()}");
            Console.WriteLine($"[12 / 95] - CMSX_DOCUMENT => {new tbl_cmsx_document().Transfer()}");
            Console.WriteLine($"[13 / 95] - CMSX_HTMLPAGE => {new tbl_cmsx_htmlpage().Transfer()}");
            Console.WriteLine($"[14 / 95] - CMSX_IMAGE => {new tbl_cmsx_image().Transfer()}");
            Console.WriteLine($"[15 / 95] - CMSX_NEWSITEM => {new tbl_cmsx_newsitem().Transfer()}");
            Console.WriteLine($"[16 / 95] - CMSX_TRADESHOW => {new tbl_cmsx_tradeshow().Transfer()}");
            Console.WriteLine($"[17 / 95] - CTLS_EXCEPTION => {new tbl_ctls_exception().Transfer()}");
            Console.WriteLine($"[18 / 95] - CTLS_EXCEPTION_REQUEST => {new tbl_ctls_exception_request().Transfer()}");
            Console.WriteLine($"[19 / 95] - CTV_MODULE => {new tbl_ctv_module().Transfer()}");
            Console.WriteLine($"[20 / 95] - CTV_VERSION => {new tbl_ctv_version().Transfer()}");
            Console.WriteLine($"[21 / 95] - CUSTOMER_EMERGENCY_BUILD_RELEASE => {new tbl_customer_emergency_build_release().Transfer()}");
            Console.WriteLine($"[22 / 95] - CUSTOMER_EXTRA_RELEASE_DOWNLOAD => {new tbl_customer_extra_release_download().Transfer()}");
            Console.WriteLine($"[23 / 95] - CUSTOMER_GROUP => {new tbl_customer_group().Transfer()}");
            Console.WriteLine($"[24 / 95] - CUSTOMER_GROUP_RELEASE_ASSIGNMENT => {new tbl_customer_group_release_assignment().Transfer()}");
            Console.WriteLine($"[25 / 95] - CUSTOMER_LICENSED_PROGRAM => {new tbl_customer_licensed_program().Transfer()}");
            Console.WriteLine($"[26 / 95] - CUSTOMER_LICENSED_PROGRAM_MODULE => {new tbl_customer_licensed_program_module().Transfer()}");
            Console.WriteLine($"[27 / 95] - CUSTOMER_RELEASE_ASSIGNMENT => {new tbl_customer_release_assignment().Transfer()}");
            Console.WriteLine($"[28 / 95] - CUSTOMER_RELEASE_ASSIGNMENT_NOTIFICATION => {new tbl_customer_release_assignment_notification().Transfer()}");
            Console.WriteLine($"[29 / 95] - CUSTOMER_RELEASE_BLOCK => {new tbl_customer_release_block().Transfer()}");
            Console.WriteLine($"[30 / 95] - DOWNLOAD => {new tbl_download().Transfer()}");
            Console.WriteLine($"[31 / 95] - LOCALES => {new tbl_locale().Transfer()}");
            Console.WriteLine($"[32 / 95] - MODULE_RELEASE_BLOCK => {new tbl_module_release_block().Transfer()}");
            Console.WriteLine($"[33 / 95] - MODULE_RELEASE_BLOCK_RULE => {new tbl_module_release_block_rule().Transfer()}");
            Console.WriteLine($"[34 / 95] - MODULE_USAGE => {new tbl_module_usage().Transfer()}");
            Console.WriteLine($"[35 / 95] - PROGRAM_FTP_MAPPING => {new tbl_program_ftp_mapping().Transfer()}");
            Console.WriteLine($"[36 / 95] - PROGRAM_GROUP => {new tbl_program_group().Transfer()}");
            Console.WriteLine($"[37 / 95] - RELEASE => {new tbl_release().Transfer()}");
            Console.WriteLine($"[38 / 95] - RELEASE_CHANGE_LOG => {new tbl_release_change_log().Transfer()}");
            Console.WriteLine($"[39 / 95] - RELEASE_NOTE_DOCUMENTATION_STATUS => {new tbl_release_note_documentation_status().Transfer()}");
            Console.WriteLine($"[40 / 95] - RELEASE_NOTE_EXCLUSIVE_COUNTRY_LOCALE => {new tbl_release_note_exclusive_country_locale().Transfer()}");
            Console.WriteLine($"[41 / 95] - RELEASE_NOTE_LOCALE_EXCLUSION => {new tbl_release_note_locale_exclusion().Transfer()}");
            Console.WriteLine($"[42 / 95] - RELEASE_STRATEGY => {new tbl_release_strategy().Transfer()}");
            Console.WriteLine($"[43 / 95] - RESOURCE_CODE => {new tbl_resource_code().Transfer()}");
            Console.WriteLine($"[44 / 95] - RESOURCE_GROUP => {new tbl_resource_group().Transfer()}");
            Console.WriteLine($"[45 / 95] - RESOURCE_TRANSLATION => {new tbl_resource_translation().Transfer()}");
            Console.WriteLine($"[46 / 95] - RESOURCE_TYPE => {new tbl_resource_type().Transfer()}");
            Console.WriteLine($"[47 / 95] - T_CUSTOMER => {new tbl_t_customer().Transfer()}");
            Console.WriteLine($"[48 / 95] - T_USER_PHONE => {new tbl_t_user_phone().Transfer()}");
            Console.WriteLine($"[49 / 95] - T_USER_PHONE_GROUP => {new tbl_t_user_phone_group().Transfer()}");
            Console.WriteLine($"[50 / 95] - T_USER_TRANSLATE => {new tbl_t_user_translate().Transfer()}");
            Console.WriteLine($"[51 / 95] - TER_ERROR_REPORT => {new tbl_ter_error_report().Transfer()}");
            Console.WriteLine($"[52 / 95] - TER_ERROR_REPORT_CUSTOMER => {new tbl_ter_error_report_customer().Transfer()}");
            Console.WriteLine($"[53 / 95] - TER_ERROR_REPORT_FILE => {new tbl_ter_error_report_file().Transfer()}");
            Console.WriteLine($"[54 / 95] - TER_ERROR_REPORT_STATUS => {new tbl_ter_error_report_status().Transfer()}");
            Console.WriteLine($"[55 / 95] - TER_ERROR_REPORT_TYPE => {new tbl_ter_error_report_type().Transfer()}");
            Console.WriteLine($"[56 / 95] - TP_ACCOUNT_REQUEST => {new tbl_tp_account_request().Transfer()}");
            Console.WriteLine($"[57 / 95] - TP_CONTACT_REQUEST => {new tbl_tp_contact_request().Transfer()}");
            Console.WriteLine($"[58 / 95] - TU_PRIVILEGE => {new tbl_tu_privilege().Transfer()}");
            Console.WriteLine($"[59 / 95] - TU_USER => {new tbl_tu_user().Transfer()}");
            Console.WriteLine($"[60 / 95] - TU_USER_AUDIT => {new tbl_tu_user_audit().Transfer()}");
            Console.WriteLine($"[61 / 95] - TV_BOOKKEEPING_SOFTWARE => {new tbl_tv_bookkeeping_software().Transfer()}");
            Console.WriteLine($"[62 / 95] - TVC_CUSTOMER_BUILD => {new tbl_tvc_customer_build().Transfer()}");
            Console.WriteLine($"[63 / 95] - TVC_CUSTOMER_DOS_FLEX => {new tbl_tvc_customer_dos_flex().Transfer()}");
            Console.WriteLine($"[64 / 95] - TVC_CUSTOMER_FLEX => {new tbl_tvc_customer_flex().Transfer()}");
            Console.WriteLine($"[65 / 95] - TVC_RELEASE_BUILD => {new tbl_tvc_release_build().Transfer()}");
            Console.WriteLine($"[66 / 95] - TVC_RELEASE_NOTE => {new tbl_tvc_release_note().Transfer()}");
            Console.WriteLine($"[67 / 95] - TVC_RELEASE_NOTE_ASSET => {new tbl_tvc_release_note_asset().Transfer()}");
            Console.WriteLine($"[68 / 95] - TVC_RELEASE_NOTE_AUDIT => {new tbl_tvc_release_note_audit().Transfer()}");
            Console.WriteLine($"[69 / 95] - TVC_RELEASE_NOTE_BUILD => {new tbl_tvc_release_note_build().Transfer()}");
            Console.WriteLine($"[70 / 95] - TVC_RELEASE_NOTE_CALL => {new tbl_tvc_release_note_call().Transfer()}");
            Console.WriteLine($"[71 / 95] - TVC_RELEASE_NOTE_COMMENT => {new tbl_tvc_release_note_comment().Transfer()}");
            Console.WriteLine($"[72 / 95] - TVC_RELEASE_NOTE_CUSTOMER => {new tbl_tvc_release_note_customer().Transfer()}");
            Console.WriteLine($"[73 / 95] - TVC_RELEASE_NOTE_ERROR_REPORT => {new tbl_tvc_release_note_error_report().Transfer()}");
            Console.WriteLine($"[74 / 95] - TVC_RELEASE_NOTE_MENU => {new tbl_tvc_release_note_menu().Transfer()}");
            Console.WriteLine($"[75 / 95] - TVC_RELEASE_NOTE_MODULE => {new tbl_tvc_release_note_module().Transfer()}");
            Console.WriteLine($"[76 / 95] - TVC_RELEASE_NOTE_NOTIFICATION => {new tbl_tvc_release_note_notification().Transfer()}");
            Console.WriteLine($"[77 / 95] - TVC_RELEASE_NOTE_NOTIFICATION_TMP => {new tbl_tvc_release_note_notification_tmp().Transfer()}");
            Console.WriteLine($"[78 / 95] - TVC_RELEASE_NOTE_PROGRAM => {new tbl_tvc_release_note_program().Transfer()}");
            Console.WriteLine($"[79 / 95] - TVC_RELEASE_NOTE_TEXT => {new tbl_tvc_release_note_text().Transfer()}");
            Console.WriteLine($"[80 / 95] - TVCS_MENU => {new tbl_tvcs_menu().Transfer()}");
            Console.WriteLine($"[81 / 95] - TVCS_MODULE => {new tbl_tvcs_module().Transfer()}");
            Console.WriteLine($"[82 / 95] - TVCS_PROGRAM => {new tbl_tvcs_program().Transfer()}");
            Console.WriteLine($"[83 / 95] - TVCS_RELEASE_NOTE_CAUSE => {new tbl_tvcs_release_note_cause().Transfer()}");
            Console.WriteLine($"[84 / 95] - TVCS_RELEASE_NOTE_DOCS_STATUS => {new tbl_tvcs_release_note_docs_status().Transfer()}");
            Console.WriteLine($"[85 / 95] - TVCS_RELEASE_NOTE_IMPACT => {new tbl_tvcs_release_note_impact().Transfer()}");
            Console.WriteLine($"[86 / 95] - TVCS_RELEASE_NOTE_NOTIFICATION_STATUS => {new tbl_tvcs_release_note_notification_status().Transfer()}");
            Console.WriteLine($"[87 / 95] - TVCS_RELEASE_NOTE_RISK => {new tbl_tvcs_release_note_risk().Transfer()}");
            Console.WriteLine($"[88 / 95] - TVCS_RELEASE_NOTE_SOURCE => {new tbl_tvcs_release_note_source().Transfer()}");
            Console.WriteLine($"[89 / 95] - TVCS_RELEASE_NOTE_STATUS => {new tbl_tvcs_release_note_status().Transfer()}");
            Console.WriteLine($"[90 / 95] - TVCS_RELEASE_NOTE_TYPE => {new tbl_tvcs_release_note_type().Transfer()}");
            Console.WriteLine($"[91 / 95] - TVCS_RELEASE_STATUS => {new tbl_tvcs_release_status().Transfer()}");
            Console.WriteLine($"[92 / 95] - USER_CHANGE_LOG => {new tbl_user_change_log().Transfer()}");
            Console.WriteLine($"[93 / 95] - USER_ROLE => {new tbl_user_role().Transfer()}");
            Console.WriteLine($"[94 / 95] - USER_ROLE_PRIVILEGE => {new tbl_user_role_privilege().Transfer()}");
            Console.WriteLine($"[95 / 95] - APPLICATION_SETTING => [SKIPPED! (0/0)]");
            Console.WriteLine("");
            Console.WriteLine("Migration has finished.");
            Console.WriteLine("");
            Console.WriteLine("[Press a key to exit]");
            Console.ReadKey();
        }
    }
}
