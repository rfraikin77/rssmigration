﻿using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_tvc_release_note_error_report : Migrator
    {
        public class ReleaseNoteErrorReport
        {
            public int NoteId { get; set; }
            public int ErrorReportId { get; set; }
        }

        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.Database.SqlQuery<ReleaseNoteErrorReport>("select * from tvc_release_note__error_report", new object[0]).ToList();
                if (entries.Count > 0 && (newContext.tvc_release_note_error_report.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table tvc_release_note_error_report", new object[0]);
                    var entityList = newContext.tvc_release_note_error_report.ToList();
                    foreach (var entry in entries)
                    {
                        var releaseNoteErrorReport = new tvc_release_note_error_report
                        {
                            note_id = entry.NoteId,
                            error_report_id = entry.ErrorReportId
                        };
                        var exists = entityList.Any(x => 
                            x.note_id == entry.NoteId && x.error_report_id == entry.ErrorReportId);
                        if (!exists)
                        {
                            entityList.Add(releaseNoteErrorReport);
                        }
                    }
                    newContext.tvc_release_note_error_report.AddRange(entityList);
                    newContext.SaveChanges();
                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.tvc_release_note_error_report.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.tvc_release_note_error_report.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
