﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_ctv_module : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entryCounty = oldContext.ctv_module.Count();
                if (entryCounty > 0 && (newContext.ctv_module.Count() < entryCounty))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table ctv_module", new object[0]);
                    SequenceHandler.DropSequenceForTable("ctv_module_id_seq");
                    
                    var entries = oldContext.ctv_module.ToList();
                    foreach (var entry in entries)
                    {
                        var ctvModule = new ctv_module
                        {
                            id = entry.moduleID,
                            name = entry.name
                        };
                        var exists = newContext.ctv_module.Any(x => x.id == entry.moduleID);
                        if (!exists)
                        {
                            newContext.ctv_module.Add(ctvModule);
                            newContext.SaveChanges();
                        }
                    }

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("ctv_module", "id", "ctv_module_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("ctv_module", "id", "ctv_module_id_seq");
                    SequenceHandler.SetSequenceForTable("ctv_module", "id", "ctv_module_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.ctv_module.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entryCounty}/{newContext.ctv_module.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
