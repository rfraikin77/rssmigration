﻿using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_cmsx_document : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.cmsx_document.ToList();
                if (entries.Count > 0 && (newContext.cmsx_document.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table cmsx_document", new object[0]);
                    var entityList = newContext.cmsx_document.ToList();
                    foreach (var entry in entries)
                    {
                        var cmsXdocument = new cmsx_document
                        {
                            revision_id = entry.revisionId,
                            path = entry.path,
                            mime_type = entry.mimetype,
                            document_size = entry.size
                        };
                        var exists = entityList.Any(x => x.revision_id == entry.revisionId);
                        if (!exists)
                        {
                            entityList.Add(cmsXdocument);
                        }
                    }
                    newContext.cmsx_document.AddRange(entityList);
                    newContext.SaveChanges();
                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.cmsx_document.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.cmsx_document.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
