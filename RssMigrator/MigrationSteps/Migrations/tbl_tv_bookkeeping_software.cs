﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_tv_bookkeeping_software : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.tv_bookkeeping_software.ToList();
                if (entries.Count > 0 && (newContext.tv_bookkeeping_software.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table tv_bookkeeping_software", new object[0]);
                    SequenceHandler.DropSequenceForTable("tv_bookkeeping_software_id_seq");
                    
                    var entityList = newContext.tv_bookkeeping_software.ToList();
                    foreach (var entry in entries)
                    {
                        var bookkeepingSoftware = new tv_bookkeeping_software
                        {
                            id = entry.bookkeepingID,
                            reflex_3000_id = entry.Reflex3000ID,
                            name = entry.name
                        };
                        var exists = entityList.Any(x => x.id == entry.bookkeepingID);
                        if (!exists)
                        {
                            entityList.Add(bookkeepingSoftware);
                        }
                    }
                    newContext.tv_bookkeeping_software.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("tv_bookkeeping_software", "id", "tv_bookkeeping_software_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("tv_bookkeeping_software", "id", "tv_bookkeeping_software_id_seq");
                    SequenceHandler.SetSequenceForTable("tv_bookkeeping_software", "id", "tv_bookkeeping_software_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.tv_bookkeeping_software.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.tv_bookkeeping_software.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
