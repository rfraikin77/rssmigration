﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_cms_department : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.cms_department.ToList();
                if (entries.Count > 0 && (newContext.cms_department.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table cms_department", new object[0]);
                    SequenceHandler.DropSequenceForTable("cms_department_id_seq");

                    var entityList = newContext.cms_department.ToList();
                    foreach (var entry in entries)
                    {
                        var department = new cms_department
                        {
                            id = entry.departmentID,
                            locale_id = entry.localeID,
                            company_name = entry.companyName,
                            company_address = entry.companyAddress,
                            company_postal_code = entry.companyPostalCode,
                            company_city = entry.companyCity,
                            company_country = entry.companyCountry,
                            company_phone = entry.companyPhone,
                            company_fax = entry.companyFax,
                            email_info = entry.emailInfo,
                            email_sales = entry.emailSales,
                            email_helpdesk = entry.emailHelpdesk,
                            internet_service_url = entry.internetServiceURL,
                            google_map_url = entry.googleMapURL,
                            google_map_x = entry.googleMapX,
                            google_map_y = entry.googleMapY,
                            is_default = entry.@default ? 1 : 0
                        };
                        var exists = entityList.Any(x => x.id == entry.departmentID);
                        if (!exists)
                        {
                            entityList.Add(department);
                        }
                    }
                    newContext.cms_department.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("cms_department", "id" ,"cms_department_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("cms_department", "id", "cms_department_id_seq");
                    SequenceHandler.SetSequenceForTable("cms_department", "id", "cms_department_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.cms_department.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.cms_department.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
