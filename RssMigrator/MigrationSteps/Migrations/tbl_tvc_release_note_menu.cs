﻿using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_tvc_release_note_menu : Migrator
    {
        public class ReleaseNoteMenu
        {
            public int NoteId { get; set; }
            public int MenuId { get; set; }
        }

        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.Database.SqlQuery<ReleaseNoteMenu>("select * from tvc_release_note__menu", new object[0]).ToList();
                if (entries.Count > 0 && (newContext.tvc_release_note_menu.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table tvc_release_note_menu", new object[0]);
                    var entityList = newContext.tvc_release_note_menu.ToList();
                    foreach (var entry in entries)
                    {
                        var releaseNoteMenu = new tvc_release_note_menu
                        {
                            note_id = entry.NoteId,
                            menu_id = entry.MenuId
                        };
                        var exists = entityList.Any(x => 
                            x.note_id == entry.NoteId && x.menu_id == entry.MenuId);
                        if (!exists)
                        {
                            entityList.Add(releaseNoteMenu);
                        }
                    }
                    newContext.tvc_release_note_menu.AddRange(entityList);
                    newContext.SaveChanges();
                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.tvc_release_note_menu.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.tvc_release_note_menu.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
