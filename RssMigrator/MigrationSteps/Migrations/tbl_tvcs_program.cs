﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_tvcs_program : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.tvcs_program.ToList();
                if (entries.Count > 0 && (newContext.tvcs_program.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table tvcs_program", new object[0]);
                    SequenceHandler.DropSequenceForTable("tvcs_program_id_seq");
                    
                    var entityList = newContext.tvcs_program.ToList();
                    foreach (var entry in entries)
                    {
                        var tvcsProgram = new tvcs_program
                        {
                            id = entry.programID,
                            code = entry.code,
                            has_menus = entry.has_menus ? 1 : 0,
                            has_modules = entry.has_modules ? 1 : 0,
                            parent_id = entry.parentID,
                            license_module_id = entry.licenseModuleID,
                            exe = entry.exe,
                            exe_old = entry.exe_old,
                            deleted = entry.deleted ? 1 : 0,
                            sort_order = entry.order,
                            is_transito_version = entry.IsTransitoVersion ? 1 : 0,
                            program_group_id = entry.ProgramGroupId,
                            release_strategy_id = entry.ReleaseStrategyId,
                            inherit_parent_license = entry.InheritParentLicense ? 1 : 0
                        };
                        var exists = entityList.Any(x => x.id == entry.programID);
                        if (!exists)
                        {
                            entityList.Add(tvcsProgram);
                        }
                    }
                    newContext.tvcs_program.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("tvcs_program", "id", "tvcs_program_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("tvcs_program", "id", "tvcs_program_id_seq");
                    SequenceHandler.SetSequenceForTable("tvcs_program", "id", "tvcs_program_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.tvcs_program.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.tvcs_program.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
