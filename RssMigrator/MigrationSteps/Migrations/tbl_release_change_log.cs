﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_release_change_log : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.ReleaseChangeLogs.ToList();
                if (entries.Count > 0 && (newContext.release_change_log.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table release_change_log cascade", new object[0]);
                    SequenceHandler.DropSequenceForTable("release_change_log_id_seq");

                    var index = 1;
                    var entityList = newContext.release_change_log.ToList();
                    foreach (var entry in entries)
                    {
                        var releaseChangeLog = new release_change_log
                        {
                            id = index,
                            release_id = entry.ReleaseId,
                            description = entry.Description,
                            date_logged = entry.DateLogged,
                            changed_by = entry.ChangedBy
                        };
                        var exists = entityList.Any(x => 
                            x.release_id == entry.ReleaseId &&
                            x.description == entry.Description &&
                            x.date_logged == entry.DateLogged &&
                            x.changed_by == entry.ChangedBy);
                        if (!exists)
                        {
                            entityList.Add(releaseChangeLog);
                        }
                        index++;
                    }
                    newContext.release_change_log.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("release_change_log", "id", "release_change_log_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("release_change_log", "id", "release_change_log_id_seq");
                    SequenceHandler.SetSequenceForTable("release_change_log", "id", "release_change_log_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.release_change_log.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.release_change_log.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
