﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_tvc_release_note_asset : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.tvc_release_note_asset.ToList();
                if (entries.Count > 0 && (newContext.tvc_release_note_asset.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table tvc_release_note_asset", new object[0]);
                    SequenceHandler.DropSequenceForTable("tvc_release_note_asset_id_seq");
                    
                    var entityList = newContext.tvc_release_note_asset.ToList();
                    foreach (var entry in entries)
                    {
                        var releaseNoteAsset = new tvc_release_note_asset
                        {
                            id = entry.assetID,
                            note_id = entry.noteID,
                            user_id = entry.userID,
                            audit_date_created = entry.audit_date_created,
                            audit_date_modified = entry.audit_date_modified,
                            path = entry.path,
                            mime_type = entry.mimetype,
                            asset_size = entry.size,
                            @internal = entry.@internal ? 1 : 0,
                            deleted = entry.deleted ? 1 : 0,
                            name = entry.name,
                            attach = entry.attach ? 1 : 0
                        };
                        var exists = entityList.Any(x => x.id == entry.assetID);
                        if (!exists)
                        {
                            entityList.Add(releaseNoteAsset);
                        }
                    }
                    newContext.tvc_release_note_asset.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("tvc_release_note_asset", "id", "tvc_release_note_asset_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("tvc_release_note_asset", "id", "tvc_release_note_asset_id_seq");
                    SequenceHandler.SetSequenceForTable("tvc_release_note_asset", "id", "tvc_release_note_asset_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.tvc_release_note_asset.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.tvc_release_note_asset.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
