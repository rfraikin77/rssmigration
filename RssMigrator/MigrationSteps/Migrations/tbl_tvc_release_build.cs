﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_tvc_release_build : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.tvc_release_build.ToList();
                if (entries.Count > 0 && (newContext.tvc_release_build.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table tvc_release_build", new object[0]);
                    SequenceHandler.DropSequenceForTable("tvc_release_build_id_seq");
                    
                    var entityList = newContext.tvc_release_build.ToList();
                    foreach (var entry in entries)
                    {
                        var releaseBuild = new tvc_release_build
                        {
                            id = entry.buildID,
                            release_id = entry.releaseID,
                            build = entry.build,
                            date_created = entry.date_created,
                            date_release = entry.date_release,
                            is_published = entry.IsPublished ? 1 : 0,
                            is_employee_notification_sent = entry.IsEmployeeNotificationSent ? 1 : 0,
                            is_implicitly_published = entry.IsImplicitlyPublished ? 1 : 0
                        };
                        var exists = entityList.Any(x => x.id == entry.buildID);
                        if (!exists)
                        {
                            entityList.Add(releaseBuild);
                        }
                    }
                    newContext.tvc_release_build.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("tvc_release_build", "id", "tvc_release_build_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("tvc_release_build", "id", "tvc_release_build_id_seq");
                    SequenceHandler.SetSequenceForTable("tvc_release_build", "id", "tvc_release_build_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.tvc_release_build.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.tvc_release_build.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
