﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_download : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entryCount = oldContext.Downloads.Count();
                if (entryCount > 0 && (newContext.downloads.Count() < entryCount))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table download", new object[0]);
                    SequenceHandler.DropSequenceForTable("download_id_seq");

                    var index = 1;
                    var entries = oldContext.Downloads.ToList();
                    var entityList = newContext.downloads.ToList();
                    foreach (var entry in entries)
                    {
                        var newDownload = new download
                        {
                            id = index,
                            user_id = entry.UserId,
                            remote_address = entry.RemoteAddress,
                            date_downloaded = entry.DateDownloaded,
                            file_downloaded = entry.FileDownloaded,
                            username = entry.UserName
                        };
                        var exists = entityList.Any(x => x.user_id == entry.UserId && 
                            x.file_downloaded == entry.FileDownloaded && 
                            x.date_downloaded == entry.DateDownloaded);
                        if (!exists)
                        {
                            entityList.Add(newDownload);
                        }
                        index++;
                    }
                    newContext.downloads.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("download", "id", "download_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("download", "id", "download_id_seq");
                    SequenceHandler.SetSequenceForTable("download", "id", "download_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.downloads.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entryCount}/{newContext.downloads.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
