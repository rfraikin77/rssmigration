﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_tvcs_release_status : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.tvcs_release_status.ToList();
                if (entries.Count > 0 && (newContext.tvcs_release_status.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table tvcs_release_status", new object[0]);
                    SequenceHandler.DropSequenceForTable("tvcs_release_status_id_seq");
                    
                    var entityList = newContext.tvcs_release_status.ToList();
                    foreach (var entry in entries)
                    {
                        var releaseStatus = new tvcs_release_status
                        {
                            id = entry.statusID,
                            code = entry.code
                        };
                        var exists = entityList.Any(x => x.id == entry.statusID);
                        if (!exists)
                        {
                            entityList.Add(releaseStatus);
                        }
                    }
                    newContext.tvcs_release_status.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("tvcs_release_status", "id", "tvcs_release_status_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("tvcs_release_status", "id", "tvcs_release_status_id_seq");
                    SequenceHandler.SetSequenceForTable("tvcs_release_status", "id", "tvcs_release_status_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.tvcs_release_status.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.tvcs_release_status.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
