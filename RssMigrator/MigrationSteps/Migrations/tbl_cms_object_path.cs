﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_cms_object_path : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.cms_object__path.ToList();
                if (entries.Count > 0 && (newContext.cms_object_path.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table cms_object_path", new object[0]);
                    SequenceHandler.DropSequenceForTable("cms_object_path_id_seq");

                    var index = 1;
                    var entityList = newContext.cms_object_path.ToList();
                    foreach (var entry in entries)
                    {
                        var cmsObjectPath = new cms_object_path
                        {
                            id = index,
                            path_id = entry.pathId,
                            object_id = entry.objectId,
                            audit_user_id = entry.audit_userId,
                            audit_date = entry.audit_date
                        };
                        var exists = entityList.Any(x => 
                            x.path_id == entry.pathId &&
                            x.object_id == entry.objectId &&
                            x.audit_user_id == entry.audit_userId &&
                            x.audit_date == entry.audit_date);
                        if (!exists)
                        {
                            entityList.Add(cmsObjectPath);
                        }
                        index++;
                    }
                    newContext.cms_object_path.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("cms_object_path", "id", "cms_object_path_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("cms_object_path", "id", "cms_object_path_id_seq");
                    SequenceHandler.SetSequenceForTable("cms_object_path", "id", "cms_object_path_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.cms_object_path.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.cms_object_path.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
