﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_release : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.Releases.ToList();
                if (entries.Count > 0 && (newContext.releases.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("delete from releases", new object[0]);
                    SequenceHandler.DropSequenceForTable("releases_id_seq");

                    var entityList = newContext.releases.ToList();
                    foreach (var entry in entries)
                    {
                        var releaseChangeLog = new release
                        {
                            id = entry.ReleaseId,
                            program_id = entry.ProgramId,
                            major = entry.Major,
                            minor = entry.Minor,
                            release_build = entry.Release1,
                            release_index = entry.ReleaseIndex,
                            auto_upgrade = entry.AutoUpgrade
                        };
                        var exists = entityList.Any(x => x.id == entry.ReleaseId);
                        if (!exists)
                        {
                            entityList.Add(releaseChangeLog);
                        }
                    }
                    newContext.releases.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("releases", "id", "releases_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("releases", "id", "releases_id_seq");
                    SequenceHandler.SetSequenceForTable("releases", "id", "releases_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.releases.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.releases.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
