﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_tvcs_release_note_risk : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.tvcs_release_note_risk.ToList();
                if (entries.Count > 0 && (newContext.tvcs_release_note_risk.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table tvcs_release_note_risk", new object[0]);
                    SequenceHandler.DropSequenceForTable("tvcs_release_note_risk_id_seq");
                    
                    var entityList = newContext.tvcs_release_note_risk.ToList();
                    foreach (var entry in entries)
                    {
                        var releaseNoteRisk = new tvcs_release_note_risk
                        {
                            id = entry.riskID,
                            code = entry.code
                        };
                        var exists = entityList.Any(x => x.id == entry.riskID);
                        if (!exists)
                        {
                            entityList.Add(releaseNoteRisk);
                        }
                    }
                    newContext.tvcs_release_note_risk.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("tvcs_release_note_risk", "id", "tvcs_release_note_risk_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("tvcs_release_note_risk", "id", "tvcs_release_note_risk_id_seq");
                    SequenceHandler.SetSequenceForTable("tvcs_release_note_risk", "id", "tvcs_release_note_risk_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.tvcs_release_note_risk.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.tvcs_release_note_risk.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
