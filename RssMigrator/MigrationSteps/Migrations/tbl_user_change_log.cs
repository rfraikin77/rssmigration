﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_user_change_log : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.User_ChangeLog.ToList();
                if (entries.Count > 0 && (newContext.user_change_log.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table user_change_log", new object[0]);
                    SequenceHandler.DropSequenceForTable("user_change_log_id_seq");

                    var index = 1;
                    var entityList = newContext.user_change_log.ToList();
                    foreach (var entry in entries)
                    {
                        var userChangeLog = new user_change_log
                        {
                            id = index,
                            user_id = entry.UserId,
                            is_emailing_enabled = entry.IsEmailingEnabled ? 1 : 0,
                            date_logged = entry.DateLogged
                        };
                        var exists = entityList.Any(x => 
                            x.user_id == entry.UserId && 
                            x.is_emailing_enabled == (entry.IsEmailingEnabled ? 1 : 0) &&
                            x.date_logged == entry.DateLogged);
                        if (!exists)
                        {
                            entityList.Add(userChangeLog);
                        }
                        index++;
                    }
                    newContext.user_change_log.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("user_change_log", "id", "user_change_log_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("user_change_log", "id", "user_change_log_id_seq");
                    SequenceHandler.SetSequenceForTable("user_change_log", "id", "user_change_log_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.user_change_log.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.user_change_log.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
