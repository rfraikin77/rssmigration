﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_locale : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entryCount = oldContext.Locales.Count();
                if (entryCount > 0 && (newContext.locales.Count() < entryCount))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table locale", new object[0]);
                    SequenceHandler.DropSequenceForTable("locale_id_seq");

                    var entries = oldContext.Locales.ToList();
                    var entityList = newContext.locales.ToList();
                    foreach (var entry in entries)
                    {
                        var newLocale = new locale
                        {
                            id = entry.LocaleId,
                            fallback_id = entry.FallbackId,
                            language_code = entry.LanguageCode,
                            language_name = entry.Language,
                            country_code = entry.CountryCode,
                            country_name = entry.Country,
                            is_default = entry.IsDefault ? 1 : 0,
                            is_active = entry.IsActive ? 1 : 0,
                            cold_fusion_locale = entry.ColdFusionLocale
                        };

                        var exists = entityList.Any(x => x.id == entry.LocaleId);
                        if (!exists)
                        {
                            entityList.Add(newLocale);
                        }
                    }
                    newContext.locales.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("locale", "id", "locale_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("locale", "id", "locale_id_seq");
                    SequenceHandler.SetSequenceForTable("locale", "id", "locale_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.locales.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entryCount}/{newContext.locales.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
