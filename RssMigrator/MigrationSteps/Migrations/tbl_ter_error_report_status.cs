﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_ter_error_report_status : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.ter_error_report_status.ToList();
                if (entries.Count > 0 && (newContext.ter_error_report_status.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table ter_error_report_status", new object[0]);
                    SequenceHandler.DropSequenceForTable("ter_error_report_status_id_seq");
                    
                    var entityList = newContext.ter_error_report_status.ToList();
                    foreach (var entry in entries)
                    {
                        var terErrorReportStatus = new ter_error_report_status
                        {
                            id = entry.statusID,
                            code = entry.code
                        };
                        var exists = entityList.Any(x => x.id == entry.statusID && x.code == entry.code);
                        if (!exists)
                        {
                            entityList.Add(terErrorReportStatus);
                        }
                    }
                    newContext.ter_error_report_status.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("ter_error_report_status", "id", "ter_error_report_status_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("ter_error_report_status", "id", "ter_error_report_status_id_seq");
                    SequenceHandler.SetSequenceForTable("ter_error_report_status", "id", "ter_error_report_status_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.ter_error_report_status.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.ter_error_report_status.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
