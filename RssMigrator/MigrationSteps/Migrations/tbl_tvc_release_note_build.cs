﻿using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_tvc_release_note_build : Migrator
    {
        public class ReleaseNoteBuild
        {
            public int NoteId { get; set; }
            public int BuildId { get; set; }
        }

        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.Database.SqlQuery<ReleaseNoteBuild>("select * from tvc_release_note__build", new object[0]).ToList();
                if (entries.Count > 0 && (newContext.tvc_release_note_build.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table tvc_release_note_build", new object[0]);
                    var entityList = newContext.tvc_release_note_build.ToList();
                    foreach (var entry in entries)
                    {
                        var releaseNoteBuild = new tvc_release_note_build
                        {
                            note_id = entry.NoteId,
                            build_id = entry.BuildId
                        };
                        var exists = entityList.Any(x => x.note_id == entry.NoteId && x.build_id == entry.BuildId);
                        if (!exists)
                        {
                            entityList.Add(releaseNoteBuild);
                        }
                    }
                    newContext.tvc_release_note_build.AddRange(entityList);
                    newContext.SaveChanges();
                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.tvc_release_note_build.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.tvc_release_note_build.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
