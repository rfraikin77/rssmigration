﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_cms_object : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.cms_object.ToList();
                if (entries.Count > 0 && (newContext.cms_object.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table cms_object", new object[0]);
                    SequenceHandler.DropSequenceForTable("cms_object_id_seq");
                    var entityList = newContext.cms_object.ToList();
                    foreach (var entry in entries)
                    {
                        var cmsObject = new cms_object
                        {
                            id = entry.objectId,
                            type_id = entry.typeId,
                            locale_id = entry.localeId,
                            audit_user_id = entry.audit_userId,
                            audit_date = entry.audit_date
                        };
                        var exists = entityList.Any(x => x.id == entry.objectId);
                        if (!exists)
                        {
                            entityList.Add(cmsObject);
                        }
                    }
                    newContext.cms_object.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("cms_object", "id", "cms_object_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("cms_object", "id", "cms_object_id_seq");
                    SequenceHandler.SetSequenceForTable("cms_object", "id", "cms_object_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.cms_object.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.cms_object.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
