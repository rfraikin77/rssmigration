﻿using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_cmsx_htmlpage : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.cmsx_htmlpage.ToList();
                if (entries.Count > 0 && (newContext.cmsx_htmlpage.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table cmsx_htmlpage", new object[0]);
                    var entityList = newContext.cmsx_htmlpage.ToList();
                    foreach (var entry in entries)
                    {
                        var cmsXhtmlPage = new cmsx_htmlpage
                        {
                            revision_id = entry.revisionId,
                            page_title = entry.title,
                            body = entry.body,
                            css = entry.css,
                            javascript = entry.javascript,
                            meta_title = entry.meta_title,
                            meta_description = entry.meta_description,
                            meta_keywords = entry.meta_keywords
                        };
                        var exists = entityList.Any(x => 
                            x.revision_id == entry.revisionId &&
                            x.page_title == entry.title &&
                            x.body == entry.body &&
                            x.css == entry.css &&
                            x.javascript == entry.javascript &&
                            x.meta_title == entry.meta_title &&
                            x.meta_description == entry.meta_description &&
                            x.meta_keywords == entry.meta_keywords);
                        if (!exists)
                        {
                            entityList.Add(cmsXhtmlPage);
                        }
                    }
                    newContext.cmsx_htmlpage.AddRange(entityList);
                    newContext.SaveChanges();
                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.cmsx_htmlpage.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.cmsx_htmlpage.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
