﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_customer_group : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.CustomerGroups.ToList();
                if (entries.Count > 0 && (newContext.customer_group.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table customer_group", new object[0]);
                    SequenceHandler.DropSequenceForTable("customer_group_id_seq");

                    foreach (var entry in entries)
                    {
                        var customerGroup = new customer_group
                        {
                            id = entry.CustomerGroupId,
                            name = entry.Name
                        };
                        var exists = newContext.customer_group.Any(x => x.id == entry.CustomerGroupId);
                        if (!exists)
                        {
                            newContext.customer_group.Add(customerGroup);
                        }
                    }

                    //Save
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("customer_group", "id", "customer_group_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("customer_group", "id", "customer_group_id_seq");
                    SequenceHandler.SetSequenceForTable("customer_group", "id", "customer_group_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.customer_group.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.customer_group.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
