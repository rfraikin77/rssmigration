﻿using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_tvc_release_note_notification : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.tvc_release_note_notification.ToList();
                if (entries.Count > 0 && (newContext.tvc_release_note_notification.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table tvc_release_note_notification", new object[0]);
                    var entityList = newContext.tvc_release_note_notification.ToList();
                    foreach (var entry in entries)
                    {
                        var releaseNoteNotification = new tvc_release_note_notification
                        {
                            note_id = entry.noteID,
                            customer_id = entry.customerID,
                            user_id = entry.userID,
                            date_created = entry.dateCreated,
                            date_notified = entry.dateNotified,
                            email_notified = entry.emailNotified,
                            disabled = entry.disabled ? 1 : 0,
                            file_error_report_id = entry.fileErrorReportID,
                            is_email_valid = entry.IsEmailValid ? 1 : 0
                        };
                        var exists = entityList.Any(x => 
                            x.note_id == entry.noteID &&
                            x.customer_id == entry.customerID &&
                            x.user_id == entry.userID &&
                            x.date_created == entry.dateCreated &&
                            x.date_notified == entry.dateNotified &&
                            x.email_notified == entry.emailNotified &&
                            x.disabled == (entry.disabled ? 1 : 0) &&
                            x.file_error_report_id == entry.fileErrorReportID &&
                            x.is_email_valid == (entry.IsEmailValid ? 1 : 0));
                        if (!exists)
                        {
                            entityList.Add(releaseNoteNotification);
                        }
                    }
                    newContext.tvc_release_note_notification.AddRange(entityList);
                    newContext.SaveChanges();
                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.tvc_release_note_notification.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.tvc_release_note_notification.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
