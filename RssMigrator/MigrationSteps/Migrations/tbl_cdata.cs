﻿using RssMigrator.NewContext;
using System;
using System.Linq;
using RssMigrator.MigrationSteps.Functions;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_cdata : Migrator
    {
        public class Cdata
        {
            public string Cfid { get; set; }
            public string App { get; set; }
            public string Data { get; set; }
        }

        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.Database.SqlQuery<Cdata>("select * from CDATA ORDER BY cfid").ToList();
                if (entries.Count > 0 && (newContext.cdatas.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table cdata", new object[0]);
                    var entityList = newContext.cdatas.ToList();
                    foreach (var entry in entries)
                    {
                        var newCdata = new cdata
                        {
                            cfid = entry.Cfid,
                            app = entry.App,
                            data = entry.Data
                        };
                        var exists = entityList.Any(x => x.cfid == entry.Cfid);
                        if (!exists)
                        {
                            entityList.Add(newCdata);
                        }
                    }
                    newContext.cdatas.AddRange(entityList);
                    newContext.SaveChanges();

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.cdatas.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.cdatas.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
