﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_resource_type : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.ResourceTypes.ToList();
                if (entries.Count > 0 && (newContext.resource_type.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table resource_type", new object[0]);
                    SequenceHandler.DropSequenceForTable("resource_type_id_seq");
                    
                    var entityList = newContext.resource_type.ToList();
                    foreach (var entry in entries)
                    {
                        var resourceType = new resource_type
                        {
                            id = entry.ResourceTypeId,
                            name = entry.Name,
                            is_multi_line = entry.IsMultiLine ? 1 : 0,
                            contains_markup = entry.ContainsMarkup ? 1 : 0,
                            display_order = entry.DisplayOrder
                        };
                        var exists = entityList.Any(x => 
                            x.id == entry.ResourceTypeId &&
                            x.name == entry.Name &&
                            x.display_order == entry.DisplayOrder);
                        if (!exists)
                        {
                            entityList.Add(resourceType);
                        }
                    }
                    newContext.resource_type.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("resource_type", "id", "resource_type_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("resource_type", "id", "resource_type_id_seq");
                    SequenceHandler.SetSequenceForTable("resource_type", "id", "resource_type_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.resource_type.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.resource_type.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
