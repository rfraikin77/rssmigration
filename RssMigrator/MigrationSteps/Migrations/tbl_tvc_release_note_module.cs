﻿using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_tvc_release_note_module : Migrator
    {
        public class ReleaseNoteModule
        {
            public int NoteId { get; set; }
            public int ModuleId { get; set; }
        }

        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.Database.SqlQuery<ReleaseNoteModule>("select * from tvc_release_note__module", new object[0]).ToList();
                if (entries.Count > 0 && (newContext.tvc_release_note_module.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table tvc_release_note_module", new object[0]);
                    var entityList = newContext.tvc_release_note_module.ToList();
                    foreach (var entry in entries)
                    {
                        var releaseNoteModule = new tvc_release_note_module
                        {
                            note_id = entry.NoteId,
                            module_id = entry.ModuleId
                        };
                        var exists = entityList.Any(x => 
                            x.note_id == entry.NoteId && x.module_id == entry.ModuleId);
                        if (!exists)
                        {
                            entityList.Add(releaseNoteModule);
                        }
                    }
                    newContext.tvc_release_note_module.AddRange(entityList);
                    newContext.SaveChanges();
                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.tvc_release_note_module.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.tvc_release_note_module.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
