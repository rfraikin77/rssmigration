﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_customer_emergency_build_release : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entryCount = oldContext.CustomerEmergencyBuildReleases.Count();
                if (entryCount > 0 && (newContext.customer_emergency_build_release.Count() < entryCount))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table customer_emergency_build_release", new object[0]);
                    SequenceHandler.DropSequenceForTable("customer_emergency_build_release_id_seq");

                    var index = 1;
                    var entries = oldContext.CustomerEmergencyBuildReleases.ToList();
                    foreach (var entry in entries)
                    {
                        var customerEmergencyBuildRelease = new customer_emergency_build_release
                        {
                            id = index,
                            customer_id = entry.CustomerId,
                            build_id = entry.BuildId,
                            date_released = entry.DateReleased
                        };
                        var exists = newContext.customer_emergency_build_release.Any(x => 
                            x.customer_id == entry.CustomerId && 
                            x.build_id == entry.BuildId &&
                            x.date_released == entry.DateReleased);
                        if (!exists)
                        {
                            newContext.customer_emergency_build_release.Add(customerEmergencyBuildRelease);
                        }
                        index++;
                    }

                    //Save
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("customer_emergency_build_release", "id", "customer_emergency_build_release_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("customer_emergency_build_release", "id", "customer_emergency_build_release_id_seq");
                    SequenceHandler.SetSequenceForTable("customer_emergency_build_release", "id", "customer_emergency_build_release_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.customer_emergency_build_release.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entryCount}/{newContext.customer_emergency_build_release.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
