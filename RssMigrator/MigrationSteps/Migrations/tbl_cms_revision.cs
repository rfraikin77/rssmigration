﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_cms_revision : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.cms_revision.ToList();
                if (entries.Count > 0 && (newContext.cms_revision.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table cms_revision", new object[0]);
                    SequenceHandler.DropSequenceForTable("cms_revision_id_seq");

                    var entityList = newContext.cms_revision.ToList();
                    foreach (var entry in entries)
                    {
                        var cmsRevision = new cms_revision
                        {
                            id = entry.revisionId,
                            object_id = entry.objectId,
                            audit_user_id = entry.audit_userId,
                            audit_date = entry.audit_date,
                            audit_modified = entry.audit_modified
                        };
                        var exists = entityList.Any(x => 
                            x.id == entry.revisionId &&
                            x.object_id == entry.objectId &&
                            x.audit_user_id == entry.audit_userId &&
                            x.audit_date == entry.audit_date &&
                            x.audit_modified == entry.audit_modified);
                        if (!exists)
                        {
                            entityList.Add(cmsRevision);
                        }
                    }
                    newContext.cms_revision.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("cms_revision", "id", "cms_revision_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("cms_revision", "id", "cms_revision_id_seq");
                    SequenceHandler.SetSequenceForTable("cms_revision", "id", "cms_revision_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.cms_revision.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.cms_revision.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
