﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_tvcs_release_note_cause : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.tvcs_release_note_cause.ToList();
                if (entries.Count > 0 && (newContext.tvcs_release_note_cause.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table tvcs_release_note_cause", new object[0]);
                    SequenceHandler.DropSequenceForTable("tvcs_release_note_cause_id_seq");
                    
                    var entityList = newContext.tvcs_release_note_cause.ToList();
                    foreach (var entry in entries)
                    {
                        var releaseNoteCause = new tvcs_release_note_cause
                        {
                            id = entry.CauseId,
                            code = entry.Code,
                            sort_order = entry.SortOrder
                        };
                        var exists = entityList.Any(x => x.id == entry.CauseId);
                        if (!exists)
                        {
                            entityList.Add(releaseNoteCause);
                        }
                    }
                    newContext.tvcs_release_note_cause.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("tvcs_release_note_cause", "id", "tvcs_release_note_cause_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("tvcs_release_note_cause", "id", "tvcs_release_note_cause_id_seq");
                    SequenceHandler.SetSequenceForTable("tvcs_release_note_cause", "id", "tvcs_release_note_cause_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.tvcs_release_note_cause.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.tvcs_release_note_cause.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
