﻿using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_user_role_privilege : Migrator
    {
        public class UserRolePrivilege
        {
            public int UserRoleId { get; set; }
            public int PrivilegeId { get; set; }
        }

        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.Database.SqlQuery<UserRolePrivilege>("select * from UserRolePrivilege", new object[0]).ToList();
                if (entries.Count > 0 && (newContext.user_role_privilege.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table user_role_privilege", new object[0]);
                    var entityList = newContext.user_role_privilege.ToList();
                    foreach (var entry in entries)
                    {
                        var userRolePrivilege = new user_role_privilege
                        {
                            user_role_id = entry.UserRoleId,
                            privilege_id = entry.PrivilegeId
                        };
                        var exists = entityList.Any(x => 
                            x.user_role_id == entry.UserRoleId && 
                            x.privilege_id == entry.PrivilegeId);
                        if (!exists)
                        {
                            entityList.Add(userRolePrivilege);
                        }
                    }
                    newContext.user_role_privilege.AddRange(entityList);
                    newContext.SaveChanges();
                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.user_role_privilege.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.user_role_privilege.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
