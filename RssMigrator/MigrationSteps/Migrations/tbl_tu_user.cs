﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_tu_user : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.tu_user.Count();
                if (entries > 0 && (newContext.tu_user.Count() < entries))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table tu_user", new object[0]);
                    SequenceHandler.DropSequenceForTable("tu_user_id_seq");
                    
                    var entityList = newContext.tu_user.ToList();
                    foreach (var entry in oldContext.tu_user.ToList())
                    {
                        var user = new tu_user
                        {
                            id = entry.userID,
                            username = entry.username,
                            password_hash = entry.password_hash,
                            password_salt = entry.password_salt,
                            email = entry.email,
                            firstname = entry.firstname,
                            lastname = entry.lastname,
                            is_active = entry.active ? 1 : 0,
                            deleted = entry.deleted ? 1 : 0,
                            preferences = entry.preferences,
                            locale_id = entry.localeID,
                            customer_id = entry.CustomerId,
                            is_ldap_copy = entry.IsLdapCopy ? 1 : 0,
                            last_ldap_validation = entry.LastLdapValidation,
                            account_expiration_date = entry.AccountExpirationDate,
                            is_account_expiration_notification_sent = entry.IsAccountExpirationNotificationSent ? 1 : 0,
                            user_role_id = entry.UserRoleId,
                            must_change_pw = 1
                        };
                        var exists = entityList.Any(x => x.id == entry.userID);
                        if (!exists)
                        {
                            entityList.Add(user);
                        }
                    }
                    newContext.tu_user.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("tu_user", "id", "tu_user_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("tu_user", "id", "tu_user_id_seq");
                    SequenceHandler.SetSequenceForTable("tu_user", "id", "tu_user_id_seq");

                    TableMigrationJournal = $" (R: {entries} / W: {newContext.tu_user.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries}/{newContext.tu_user.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
