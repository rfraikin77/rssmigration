﻿using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_cmsx_image : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.cmsx_image.ToList();
                if (entries.Count > 0 && (newContext.cmsx_image.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table cmsx_image", new object[0]);
                    var entityList = newContext.cmsx_image.ToList();
                    foreach (var entry in entries)
                    {
                        var cmsXimage = new cmsx_image
                        {
                            revision_id = entry.revisionId,
                            path = entry.path,
                            mime_type = entry.mimetype,
                            width = entry.width,
                            height = entry.height,
                            image_size = entry.size
                        };
                        var exists = entityList.Any(x => 
                            x.revision_id == entry.revisionId &&
                            x.path == entry.path &&
                            x.mime_type == entry.mimetype &&
                            x.width == entry.width &&
                            x.height == entry.height &&
                            x.image_size == entry.size);
                        if (!exists)
                        {
                            entityList.Add(cmsXimage);
                        }
                    }
                    newContext.cmsx_image.AddRange(entityList);
                    newContext.SaveChanges();
                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.cmsx_image.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.cmsx_image.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
