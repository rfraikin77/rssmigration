﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_tvc_release_note_audit : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.tvc_release_note_audit.ToList();
                if (entries.Count > 0 && (newContext.tvc_release_note_audit.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table tvc_release_note_audit", new object[0]);
                    SequenceHandler.DropSequenceForTable("tvc_release_note_audit_id_seq");

                    var index = 1;
                    var entityList = newContext.tvc_release_note_audit.ToList();
                    foreach (var entry in entries)
                    {
                        var releaseNoteAudit = new tvc_release_note_audit
                        {
                            id = index,
                            note_id = entry.noteID,
                            status_id = entry.statusID,
                            user_id = entry.userID,
                            audit_date = entry.audit_date
                        };
                        entityList.Add(releaseNoteAudit);
                        index++;
                    }
                    newContext.tvc_release_note_audit.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("tvc_release_note_audit", "id", "tvc_release_note_audit_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("tvc_release_note_audit", "id", "tvc_release_note_audit_id_seq");
                    SequenceHandler.SetSequenceForTable("tvc_release_note_audit", "id", "tvc_release_note_audit_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.tvc_release_note_audit.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.tvc_release_note_audit.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
