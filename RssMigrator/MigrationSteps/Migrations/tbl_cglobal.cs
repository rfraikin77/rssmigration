﻿using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_cglobal : Migrator
    {
        public class Cglobal
        {
            public string Cfid { get; set; }
            public DateTime? Lvisit { get; set; }
            public string Data { get; set; }
        }

        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.Database.SqlQuery<Cglobal>("select * from CGLOBAL ORDER BY cfid").ToList();
                if (entries.Count > 0 && (newContext.cglobals.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table cglobal", new object[0]);
                    var entityList = newContext.cglobals.ToList();
                    foreach (var entry in entries)
                    {
                        var newCglobal = new cglobal
                        {
                            cfid = entry.Cfid,
                            lvisit = entry.Lvisit,
                            data = entry.Data
                        };
                        var exists = entityList.Any(x => x.cfid == entry.Cfid);
                        if (!exists)
                        {
                            entityList.Add(newCglobal);
                        }
                    }
                    newContext.cglobals.AddRange(entityList);
                    newContext.SaveChanges();
                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.cglobals.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.cglobals.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
