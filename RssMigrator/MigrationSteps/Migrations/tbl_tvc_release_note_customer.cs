﻿using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_tvc_release_note_customer : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.tvc_release_note__customer.ToList();
                if (entries.Count > 0 && (newContext.tvc_release_note_customer.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table tvc_release_note_customer", new object[0]);
                    var entityList = newContext.tvc_release_note_customer.ToList();
                    foreach (var entry in entries)
                    {
                        var releaseNoteCustomer = new tvc_release_note_customer
                        {
                            note_id = entry.noteID,
                            customer_id = entry.customerID,
                            date_created = entry.dateCreated,
                            added_by_error_report = entry.addedByErrorReport ? 1 : 0,
                            external_reference = entry.ExternalReference
                        };
                        var exists = entityList.Any(x => 
                            x.note_id == entry.noteID &&
                            x.customer_id ==entry.customerID &&
                            x.date_created == entry.dateCreated &&
                            x.added_by_error_report == (entry.addedByErrorReport ? 1 : 0) &&
                            x.external_reference == entry.ExternalReference);
                        if (!exists)
                        {
                            entityList.Add(releaseNoteCustomer);
                        }
                    }
                    newContext.tvc_release_note_customer.AddRange(entityList);
                    newContext.SaveChanges();
                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.tvc_release_note_customer.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.tvc_release_note_customer.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
