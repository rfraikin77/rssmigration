﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_resource_code : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.ResourceCodes.ToList();
                if (entries.Count > 0 && (newContext.resource_code.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table resource_code", new object[0]);
                    SequenceHandler.DropSequenceForTable("resource_code_id_seq");
                    
                    var entityList = newContext.resource_code.ToList();
                    foreach (var entry in entries)
                    {
                        var resourceCode = new resource_code
                        {
                            id = entry.ResourceCodeId,
                            resource_group_id = entry.ResourceGroupId,
                            resource_type_id = entry.ResourceTypeId,
                            name = entry.Name
                        };
                        var exists = entityList.Any(x => 
                            x.id == entry.ResourceCodeId &&
                            x.resource_group_id == entry.ResourceGroupId &&
                            x.resource_type_id == entry.ResourceTypeId &&
                            x.name == entry.Name);
                        if (!exists)
                        {
                            entityList.Add(resourceCode);
                        }
                    }
                    newContext.resource_code.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("resource_code", "id", "resource_code_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("resource_code", "id", "resource_code_id_seq");
                    SequenceHandler.SetSequenceForTable("resource_code", "id", "resource_code_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.resource_code.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.resource_code.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
