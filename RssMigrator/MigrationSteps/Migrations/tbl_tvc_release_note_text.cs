﻿using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_tvc_release_note_text : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.tvc_release_note_text.ToList();
                if (entries.Count > 0 && (newContext.tvc_release_note_text.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table tvc_release_note_text", new object[0]);
                    var entityList = newContext.tvc_release_note_text.ToList();
                    foreach (var entry in entries)
                    {
                        var releaseNoteText = new tvc_release_note_text
                        {
                            note_id = entry.noteID,
                            locale_id = entry.localeID,
                            text_title = entry.title,
                            solution_description = entry.SolutionDescription,
                            date_created = entry.dateCreated,
                            problem_description = entry.ProblemDescription
                        };
                        var exists = entityList.Any(x => 
                            x.note_id == entry.noteID &&
                            x.locale_id == entry.localeID &&
                            x.text_title == entry.title &&
                            x.solution_description == entry.SolutionDescription &&
                            x.date_created == entry.dateCreated &&
                            x.problem_description == entry.ProblemDescription);
                        if (!exists)
                        {
                            entityList.Add(releaseNoteText);
                        }
                    }
                    newContext.tvc_release_note_text.AddRange(entityList);
                    newContext.SaveChanges();
                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.tvc_release_note_text.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.tvc_release_note_text.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
