﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_customer_release_assignment : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.CustomerReleaseAssignments.ToList();
                if (entries.Count > 0 && (newContext.customer_release_assignment.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table customer_release_assignment", new object[0]);
                    SequenceHandler.DropSequenceForTable("customer_release_assignment_id_seq");

                    var entityList = newContext.customer_release_assignment.ToList();
                    foreach (var entry in entries)
                    {
                        var customerReleaseAssignment = new customer_release_assignment
                        {
                            id = entry.CustomerReleaseAssignmentId,
                            customer_id = entry.CustomerId,
                            release_id = entry.ReleaseId,
                            assignment_date = entry.AssignmentDate,
                            assigned_by = entry.AssignedBy
                        };
                        var exists = entityList.Any(x => x.id == entry.CustomerReleaseAssignmentId);
                        if (!exists)
                        {
                            entityList.Add(customerReleaseAssignment);
                        }
                    }
                    newContext.customer_release_assignment.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("customer_release_assignment", "id", "customer_release_assignment_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("customer_release_assignment", "id", "customer_release_assignment_id_seq");
                    SequenceHandler.SetSequenceForTable("customer_release_assignment", "id", "customer_release_assignment_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.customer_release_assignment.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.customer_release_assignment.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
