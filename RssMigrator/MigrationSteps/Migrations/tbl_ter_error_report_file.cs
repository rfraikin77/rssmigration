﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_ter_error_report_file : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.ter_error_report_file.Count();
                if (entries > 0 && (newContext.ter_error_report_file.Count() < entries))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table ter_error_report_file", new object[0]);
                    SequenceHandler.DropSequenceForTable("ter_error_report_file_id_seq");
                    
                    var entityList = newContext.ter_error_report_file.ToList();
                    var originList = oldContext.ter_error_report_file.OrderBy(x => x.fileErrorReportID).ToList();
                    foreach (var entry in originList)
                    {
                        var terErrorReportCustomer = new ter_error_report_file
                        {
                            id = entry.fileErrorReportID,
                            customer_error_report_id = entry.customerErrorReportID,
                            path = entry.path,
                            file_size = entry.size,
                            date_created = entry.dateCreated,
                            computer_name = entry.computerName,
                            username = entry.userName,
                            administration_id = entry.administrationID,
                            administration_name = entry.administrationName,
                            administration_path = entry.administrationPath
                        };
                        entityList.Add(terErrorReportCustomer);

                        if (entityList.Count > 2_500_000)
                        {
                            newContext.ter_error_report_file.AddRange(entityList);
                            newContext.SaveChanges();
                            entityList = new System.Collections.Generic.List<ter_error_report_file>();
                        }
                    }
                    newContext.ter_error_report_file.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("ter_error_report_file", "id", "ter_error_report_file_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("ter_error_report_file", "id", "ter_error_report_file_id_seq");
                    SequenceHandler.SetSequenceForTable("ter_error_report_file", "id", "ter_error_report_file_id_seq");

                    TableMigrationJournal = $" (R: {entries} / W: {newContext.ter_error_report_file.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries}/{newContext.ter_error_report_file.Count()})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
