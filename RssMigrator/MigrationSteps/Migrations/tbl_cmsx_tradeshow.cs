﻿using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_cmsx_tradeshow : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entryCount = oldContext.cmsx_tradeshow.Count();
                if (entryCount > 0 && (newContext.cmsx_tradeshow.Count() < entryCount))
                {
                    var entries = oldContext.cmsx_tradeshow.ToList();
                    newContext.Database.ExecuteSqlCommand("truncate table cmsx_tradeshow", new object[0]);
                    var entityList = newContext.cmsx_tradeshow.ToList();
                    foreach (var entry in entries)
                    {
                        var cmsXtradeShow = new cmsx_tradeshow
                        {
                            revision_id = entry.revisionId,
                            name = entry.name,
                            location = entry.location,
                            start_date = entry.startdate,
                            end_date = entry.enddate,
                            admission = entry.admission,
                            stand = entry.stand,
                            info = entry.info,
                            info_url = entry.infoURL,
                            report_url = entry.reportURL
                        };
                        var exists = entityList.Any(x => 
                            x.revision_id == entry.revisionId &&
                            x.name == entry.name &&
                            x.location == entry.location &&
                            x.start_date == entry.startdate &&
                            x.end_date == entry.enddate &&
                            x.admission == entry.admission &&
                            x.stand == entry.stand &&
                            x.info == entry.info &&
                            x.info_url == entry.infoURL &&
                            x.report_url == entry.reportURL);
                        if (!exists)
                        {
                            entityList.Add(cmsXtradeShow);
                        }
                    }
                    newContext.cmsx_tradeshow.AddRange(entityList);
                    newContext.SaveChanges();
                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.cmsx_tradeshow.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entryCount}/{newContext.cmsx_tradeshow.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
