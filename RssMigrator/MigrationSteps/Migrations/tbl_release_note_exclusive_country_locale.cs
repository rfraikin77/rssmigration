﻿using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_release_note_exclusive_country_locale : Migrator
    {
        public class ReleaseNoteExclusiveCountryLocale
        {
            public int ReleaseNoteId { get; set; }
            public int ExclusiveCountryLocaleId { get; set; }
        }

        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.Database.SqlQuery<ReleaseNoteExclusiveCountryLocale>("select * from ReleaseNoteExclusiveCountryLocale", new object[0]).ToList();
                if (entries.Count > 0 && (newContext.release_note_exclusive_country_locale.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table release_note_exclusive_country_locale", new object[0]);
                    var entityList = newContext.release_note_exclusive_country_locale.ToList();
                    foreach (var entry in entries)
                    {
                        var releaseNoteExclusiveCountryLocale = new release_note_exclusive_country_locale
                        {
                            release_note_id = entry.ReleaseNoteId,
                            exclusive_country_locale_id = entry.ExclusiveCountryLocaleId
                        };
                        var exists = entityList.Any(x => 
                            x.release_note_id == entry.ReleaseNoteId && 
                            x.exclusive_country_locale_id == entry.ExclusiveCountryLocaleId);
                        if (!exists)
                        {
                            entityList.Add(releaseNoteExclusiveCountryLocale);
                        }
                    }
                    newContext.release_note_exclusive_country_locale.AddRange(entityList);
                    newContext.SaveChanges();
                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.release_note_exclusive_country_locale.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.release_note_exclusive_country_locale.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
