﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_ter_error_report_customer : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.ter_error_report_customer.OrderBy(x => x.customerErrorReportID).ToList();
                if (entries.Count > 0 && (newContext.ter_error_report_customer.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table ter_error_report_customer", new object[0]);
                    SequenceHandler.DropSequenceForTable("ter_error_report_customer_id_seq");
                    
                    var entityList = newContext.ter_error_report_customer.ToList();
                    foreach (var entry in entries)
                    {
                        var terErrorReportCustomer = new ter_error_report_customer
                        {
                            id = entry.customerErrorReportID,
                            error_report_id = entry.errorReportID,
                            major = entry.major,
                            minor = entry.minor,
                            build = entry.build,
                            customer_id = entry.customerID,
                            customer_name = entry.customerName,
                            license = entry.license
                        };
                        entityList.Add(terErrorReportCustomer);
                        if (entityList.Count > 50_000)
                        {
                            newContext.ter_error_report_customer.AddRange(entityList);
                            newContext.SaveChanges();
                            entityList = new System.Collections.Generic.List<ter_error_report_customer>();
                        }
                    }
                    newContext.ter_error_report_customer.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("ter_error_report_customer", "id", "ter_error_report_customer_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("ter_error_report_customer", "id", "ter_error_report_customer_id_seq");
                    SequenceHandler.SetSequenceForTable("ter_error_report_customer", "id", "ter_error_report_customer_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.ter_error_report_customer.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.ter_error_report_customer.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
