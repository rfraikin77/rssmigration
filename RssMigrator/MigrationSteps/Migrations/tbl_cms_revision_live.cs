﻿using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_cms_revision_live : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.cms_revision_live.ToList();
                if (entries.Count > 0 && (newContext.cms_revision_live.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table cms_revision_live", new object[0]);
                    var entityList = newContext.cms_revision_live.ToList();
                    foreach (var entry in entries)
                    {
                        var cmsRevisionLive = new cms_revision_live
                        {
                            object_id = entry.objectId,
                            revision_id = entry.revisionId,
                            audit_user_id = entry.audit_userId,
                            audit_date = entry.audit_date,
                        };
                        var exists = entityList.Any(x => 
                            x.object_id == entry.objectId &&
                            x.revision_id == entry.revisionId &&
                            x.audit_user_id == entry.audit_userId &&
                            x.audit_date == entry.audit_date);
                        if (!exists)
                        {
                            entityList.Add(cmsRevisionLive);
                        }
                    }
                    newContext.cms_revision_live.AddRange(entityList);
                    newContext.SaveChanges();
                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.cms_revision_live.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.cms_revision_live.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
