﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_ctv_version : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entryCount = oldContext.ctv_version.Count();
                if (entryCount > 0 && (newContext.ctv_version.Count() < entryCount))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table ctv_version", new object[0]);
                    SequenceHandler.DropSequenceForTable("ctv_version_id_seq");

                    var entries = oldContext.ctv_version.ToList();
                    foreach (var entry in entries)
                    {
                        var ctvVersion = new ctv_version
                        {
                            id = entry.versionID,
                            module_id = entry.moduleID,
                            major = entry.major,
                            minor = entry.minor,
                            build = entry.build,
                            audit_date = entry.audit_date
                        };
                        var exists = newContext.ctv_version.Any(x => x.id == entry.moduleID);
                        if (!exists)
                        {
                            newContext.ctv_version.Add(ctvVersion);
                            newContext.SaveChanges();
                        }
                    }

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("ctv_version", "id", "ctv_version_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("ctv_version", "id", "ctv_version_id_seq");
                    SequenceHandler.SetSequenceForTable("ctv_version", "id", "ctv_version_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.ctv_version.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entryCount}/{newContext.ctv_version.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
