﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_cms_path : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.cms_path.ToList();
                if (entries.Count > 0 && (newContext.cms_path.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table cms_path", new object[0]);
                    SequenceHandler.DropSequenceForTable("cms_path_id_seq");

                    var entityList = newContext.cms_path.ToList();
                    foreach (var entry in entries)
                    {
                        var cmsPath = new cms_path
                        {
                            id = entry.pathId,
                            parent_path_id = entry.parentPathId,
                            locale_id = entry.localeId,
                            name = entry.name
                        };
                        var exists = entityList.Any(x => 
                            x.id == entry.pathId &&
                            x.parent_path_id == entry.parentPathId &&
                            x.locale_id == entry.localeId &&
                            x.name == entry.name);
                        if (!exists)
                        {
                            entityList.Add(cmsPath);
                        }
                    }
                    newContext.cms_path.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("cms_path", "id", "cms_path_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("cms_path", "id", "cms_path_id_seq");
                    SequenceHandler.SetSequenceForTable("cms_path", "id", "cms_path_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.cms_path.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.cms_path.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
