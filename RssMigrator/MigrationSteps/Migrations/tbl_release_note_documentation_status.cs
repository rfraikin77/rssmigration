﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_release_note_documentation_status : Migrator
    {
        public class ReleaseNoteDocumentationStatus
        {
            public int DocumentationStatusId { get; set; }
            public int ReleaseNoteId { get; set; }
            public int? MenuId { get; set; }
            public int LocaleId { get; set; }
        }

        public string Transfer()
        {
            try
            {
                var entries = oldContext.Database.SqlQuery<ReleaseNoteDocumentationStatus>("select * from ReleaseNoteDocumentationStatus", new object[0]).ToList();
                if (entries.Count > 0 && (newContext.release_note_documentation_status.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table release_note_documentation_status cascade", new object[0]);
                    SequenceHandler.DropSequenceForTable("release_note_documentation_status_id_seq");

                    var index = 1;
                    var entityList = newContext.release_note_documentation_status.ToList();
                    foreach (var entry in entries)
                    {
                        var releaseNoteDocumentationStatus = new release_note_documentation_status
                        {
                            id = index,
                            documentation_status_id = entry.DocumentationStatusId,
                            release_note_id = entry.ReleaseNoteId,
                            locale_id = entry.LocaleId,
                            menu_id = entry.MenuId
                        };
                        entityList.Add(releaseNoteDocumentationStatus);
                        index++;
                    }
                    newContext.release_note_documentation_status.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("release_note_documentation_status", "id", "release_note_documentation_status_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("release_note_documentation_status", "id", "release_note_documentation_status_id_seq");
                    SequenceHandler.SetSequenceForTable("release_note_documentation_status", "id", "release_note_documentation_status_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.release_note_documentation_status.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.release_note_documentation_status.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
