﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_tvcs_release_note_source : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.tvcs_release_note_source.ToList();
                if (entries.Count > 0 && (newContext.tvcs_release_note_source.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table tvcs_release_note_source", new object[0]);
                    SequenceHandler.DropSequenceForTable("tvcs_release_note_source_id_seq");
                    
                    var entityList = newContext.tvcs_release_note_source.ToList();
                    foreach (var entry in entries)
                    {
                        var releaseNoteSource = new tvcs_release_note_source
                        {
                            id = entry.sourceID,
                            code = entry.code,
                            selectable = entry.selectable ? 1 : 0
                        };
                        var exists = entityList.Any(x => x.id == entry.sourceID);
                        if (!exists)
                        {
                            entityList.Add(releaseNoteSource);
                        }
                    }
                    newContext.tvcs_release_note_source.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("tvcs_release_note_source", "id", "tvcs_release_note_source_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("tvcs_release_note_source", "id", "tvcs_release_note_source_id_seq");
                    SequenceHandler.SetSequenceForTable("tvcs_release_note_source", "id", "tvcs_release_note_source_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.tvcs_release_note_source.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.tvcs_release_note_source.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
