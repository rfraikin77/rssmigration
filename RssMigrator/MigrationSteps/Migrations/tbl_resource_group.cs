﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_resource_group : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.ResourceGroups.ToList();
                if (entries.Count > 0 && (newContext.resource_group.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table resource_group", new object[0]);
                    SequenceHandler.DropSequenceForTable("resource_group_id_seq");
                    
                    var entityList = newContext.resource_group.ToList();
                    foreach (var entry in entries)
                    {
                        var resourceGroup = new resource_group
                        {
                            id = entry.ResourceGroupId,
                            name = entry.Name
                        };
                        var exists = entityList.Any(x => 
                            x.id == entry.ResourceGroupId &&
                            x.name == entry.Name);
                        if (!exists)
                        {
                            entityList.Add(resourceGroup);
                        }
                    }
                    newContext.resource_group.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("resource_group", "id", "resource_group_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("resource_group", "id", "resource_group_id_seq");
                    SequenceHandler.SetSequenceForTable("resource_group", "id", "resource_group_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.resource_group.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.resource_group.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
