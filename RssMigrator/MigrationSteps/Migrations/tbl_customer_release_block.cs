﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_customer_release_block : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.CustomerReleaseBlocks.ToList();
                if (entries.Count > 0 && (newContext.customer_release_block.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table customer_release_block", new object[0]);
                    SequenceHandler.DropSequenceForTable("customer_release_block_id_seq");

                    var index = 1;
                    var entityList = newContext.customer_release_block.ToList();
                    foreach (var entry in entries)
                    {
                        var customerReleaseBlock = new customer_release_block
                        {
                            id = index,
                            customer_id = entry.CustomerId,
                            blocked_from_release_id = entry.BlockedFromReleaseId,
                            blocked_until_release_id = entry.BlockedUntilReleaseId,
                            reason = entry.Reason,
                            blocked_on = entry.BlockedOn,
                            blocked_by = entry.BlockedBy
                        };
                        var exists = entityList.Any(x => 
                            x.customer_id == entry.CustomerId && 
                            x.blocked_from_release_id == entry.BlockedFromReleaseId &&
                            x.blocked_until_release_id == entry.BlockedUntilReleaseId &&
                            x.reason == entry.Reason &&
                            x.blocked_on == entry.BlockedOn &&
                            x.blocked_by == entry.BlockedBy);
                        if (!exists)
                        {
                            entityList.Add(customerReleaseBlock);
                        }
                        index++;
                    }
                    newContext.customer_release_block.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("customer_release_block", "id", "customer_release_block_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("customer_release_block", "id", "customer_release_block_id_seq");
                    SequenceHandler.SetSequenceForTable("customer_release_block", "id", "customer_release_block_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.customer_release_block.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.customer_release_block.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
