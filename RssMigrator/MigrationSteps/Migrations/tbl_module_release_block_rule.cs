﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_module_release_block_rule : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.ModuleReleaseBlockRules.ToList();
                if (entries.Count > 0 && (newContext.module_release_block_rule.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table module_release_block_rule", new object[0]);
                    SequenceHandler.DropSequenceForTable("module_release_block_rule_id_seq");

                    foreach (var entry in entries)
                    {
                        var moduleReleaseBlockRule = new module_release_block_rule
                        {
                            id = entry.RuleId,
                            rule_type_id = entry.RuleTypeId,
                            module_id_a = entry.ModuleIdA,
                            module_id_b = entry.ModuleIdB,
                            child_rule_id_a = entry.ChildRuleIdA,
                            child_rule_id_b = entry.ChildRuleIdB
                        };
                        var exists = newContext.module_release_block_rule.Any(x => x.id == entry.RuleId);
                        if (!exists)
                        {
                            newContext.module_release_block_rule.Add(moduleReleaseBlockRule);
                            newContext.SaveChanges();
                        }
                    }

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("module_release_block_rule", "id", "module_release_block_rule_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("module_release_block_rule", "id", "module_release_block_rule_id_seq");
                    SequenceHandler.SetSequenceForTable("module_release_block_rule", "id", "module_release_block_rule_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.module_release_block_rule.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.module_release_block_rule.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
