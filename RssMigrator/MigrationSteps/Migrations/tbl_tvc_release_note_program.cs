﻿using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_tvc_release_note_program : Migrator
    {
        public class ReleaseNoteProgram
        {
            public int NoteId { get; set; }
            public int ProgramId { get; set; }
        }

        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.Database.SqlQuery<ReleaseNoteProgram>("select * from tvc_release_note__program", new object[0]).ToList();
                if (entries.Count > 0 && (newContext.tvc_release_note_program.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table tvc_release_note_program", new object[0]);
                    var entityList = newContext.tvc_release_note_program.ToList();
                    foreach (var entry in entries)
                    {
                        var releaseNoteModule = new tvc_release_note_program
                        {
                            note_id = entry.NoteId,
                            program_id = entry.ProgramId
                        };
                        var exists = entityList.Any(x => 
                            x.note_id == entry.NoteId && x.program_id == entry.ProgramId);
                        if (!exists)
                        {
                            entityList.Add(releaseNoteModule);
                        }
                    }
                    newContext.tvc_release_note_program.AddRange(entityList);
                    newContext.SaveChanges();
                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.tvc_release_note_program.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.tvc_release_note_program.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
