﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_ter_error_report_type : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.ter_error_report_type.ToList();
                if (entries.Count > 0 && (newContext.ter_error_report_type.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table ter_error_report_type", new object[0]);
                    SequenceHandler.DropSequenceForTable("ter_error_report_type_id_seq");
                    
                    var entityList = newContext.ter_error_report_type.ToList();
                    foreach (var entry in entries)
                    {
                        var terErrorReportType = new ter_error_report_type
                        {
                            id = entry.typeID,
                            code = entry.code
                        };
                        var exists = entityList.Any(x => x.id == entry.typeID && x.code == entry.code);
                        if (!exists)
                        {
                            entityList.Add(terErrorReportType);
                        }
                    }
                    newContext.ter_error_report_type.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("ter_error_report_type", "id", "ter_error_report_type_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("ter_error_report_type", "id", "ter_error_report_type_id_seq");
                    SequenceHandler.SetSequenceForTable("ter_error_report_type", "id", "ter_error_report_type_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.ter_error_report_type.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.ter_error_report_type.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
