﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_ter_error_report : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.ter_error_report.OrderBy(x => x.errorReportID).ToList();
                if (entries.Count > 0 && (newContext.ter_error_report.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table ter_error_report", new object[0]);
                    SequenceHandler.DropSequenceForTable("ter_error_report_id_seq");
                    
                    var entityList = newContext.ter_error_report.ToList();
                    foreach (var entry in entries)
                    {
                        var terErrorReport = new ter_error_report
                        {
                            id = entry.errorReportID,
                            program_id = entry.programID,
                            program_name = entry.programName,
                            name = entry.name,
                            error_number = entry.number,
                            description = entry.description,
                            reoccuring_error_report_id = entry.reoccuringErrorReportID,
                            date_created = entry.dateCreated,
                            parent_name = entry.parentName,
                            engine_name = entry.engineName,
                            location = entry.location,
                            status = entry.status,
                            operation = entry.operation,
                            status_date = entry.statusDate,
                            status_description = entry.statusDescription,
                            type_id = entry.typeID,
                            status_id = entry.statusID,
                            solved_in_build_id = entry.SolvedInBuildId,
                            report_hash = entry.ReportHash
                        };
                        entityList.Add(terErrorReport);
                    }
                    newContext.ter_error_report.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("ter_error_report", "id", "ter_error_report_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("ter_error_report", "id", "ter_error_report_id_seq");
                    SequenceHandler.SetSequenceForTable("ter_error_report", "id", "ter_error_report_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.ter_error_report.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.ter_error_report.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
