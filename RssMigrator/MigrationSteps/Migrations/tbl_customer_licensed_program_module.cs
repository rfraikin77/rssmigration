﻿using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_customer_licensed_program_module : Migrator
    {
        public class CustomerLicensedProgramModule
        {
            public int CustomerId { get; set; }
            public int ModuleId { get; set; }
        }

        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.Database.SqlQuery<CustomerLicensedProgramModule>("select * from CustomerLicensedProgramModule", new object[0]).ToList();
                if (entries.Count > 0 && (newContext.customer_licensed_program_module.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table customer_licensed_program_module", new object[0]);

                    var entityList = newContext.customer_licensed_program_module.ToList();
                    foreach (var entry in entries)
                    {
                        var customerLicensedProgramModule = new customer_licensed_program_module
                        {
                            customer_id = entry.CustomerId,
                            module_id = entry.ModuleId
                        };
                        var exists = entityList.Any(x => x.customer_id == entry.CustomerId && x.module_id == entry.ModuleId);
                        if (!exists)
                        {
                            entityList.Add(customerLicensedProgramModule);
                        }
                    }
                    newContext.customer_licensed_program_module.AddRange(entityList);
                    newContext.SaveChanges();

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.customer_licensed_program_module.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.customer_licensed_program_module.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
