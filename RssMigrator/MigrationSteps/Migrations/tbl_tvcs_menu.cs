﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_tvcs_menu : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.tvcs_menu.ToList();
                if (entries.Count > 0 && (newContext.tvcs_menu.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table tvcs_menu", new object[0]);
                    SequenceHandler.DropSequenceForTable("tvcs_menu_id_seq");
                    
                    var entityList = newContext.tvcs_menu.ToList();
                    foreach (var entry in entries)
                    {
                        var tvcsMenu = new tvcs_menu
                        {
                            id = entry.menuID,
                            program_id = entry.programID,
                            parent_id = entry.parentID,
                            code = entry.code,
                            context_id = entry.contextID,
                            menu_index = entry.index,
                            main_menu = entry.mainmenu ? 1 : 0,
                            lower_build_id = entry.LowerBuildId,
                            upper_build_id = entry.UpperBuildId
                        };
                        var exists = entityList.Any(x => x.id == entry.menuID);
                        if (!exists)
                        {
                            entityList.Add(tvcsMenu);
                        }
                    }
                    newContext.tvcs_menu.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("tvcs_menu", "id", "tvcs_menu_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("tvcs_menu", "id", "tvcs_menu_id_seq");
                    SequenceHandler.SetSequenceForTable("tvcs_menu", "id", "tvcs_menu_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.tvcs_menu.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.tvcs_menu.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
