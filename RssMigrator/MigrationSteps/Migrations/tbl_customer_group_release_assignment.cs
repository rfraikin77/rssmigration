﻿using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_customer_group_release_assignment : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.CustomerGroupReleaseAssignments.ToList();
                if (entries.Count > 0 && (newContext.customer_group_release_assignment.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table customer_group_release_assignment", new object[0]);

                    foreach (var entry in entries)
                    {
                        var customerGroupReleaseAssignment = new customer_group_release_assignment
                        {
                            customer_group_id = entry.CustomerGroupId,
                            release_id = entry.ReleaseId,
                            assignment_date = entry.AssignmentDate,
                            assigned_by = entry.AssignedBy
                        };
                        var exists = newContext.customer_group_release_assignment.Any(x => x.customer_group_id == entry.CustomerGroupId && x.release_id == entry.ReleaseId);
                        if (!exists)
                        {
                            newContext.customer_group_release_assignment.Add(customerGroupReleaseAssignment);
                            newContext.SaveChanges();
                        }
                    }
                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.customer_group_release_assignment.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.customer_group_release_assignment.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
