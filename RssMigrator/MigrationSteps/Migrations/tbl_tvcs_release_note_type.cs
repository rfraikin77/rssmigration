﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_tvcs_release_note_type : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.tvcs_release_note_type.ToList();
                if (entries.Count > 0 && (newContext.tvcs_release_note_type.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table tvcs_release_note_type", new object[0]);
                    SequenceHandler.DropSequenceForTable("tvcs_release_note_type_id_seq");
                    
                    var entityList = newContext.tvcs_release_note_type.ToList();
                    foreach (var entry in entries)
                    {
                        var releaseNoteType = new tvcs_release_note_type
                        {
                            id = entry.typeID,
                            code = entry.code,
                            sort_order = entry.order,
                            selectable = entry.selectable ? 1 : 0
                        };
                        var exists = entityList.Any(x => x.id == entry.typeID);
                        if (!exists)
                        {
                            entityList.Add(releaseNoteType);
                        }
                    }
                    newContext.tvcs_release_note_type.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("tvcs_release_note_type", "id", "tvcs_release_note_type_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("tvcs_release_note_type", "id", "tvcs_release_note_type_id_seq");
                    SequenceHandler.SetSequenceForTable("tvcs_release_note_type", "id", "tvcs_release_note_type_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.tvcs_release_note_type.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.tvcs_release_note_type.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
