﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_user_role : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.UserRoles.ToList();
                if (entries.Count > 0 && (newContext.user_role.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table user_role", new object[0]);
                    SequenceHandler.DropSequenceForTable("user_role_id_seq");
                    
                    var entityList = newContext.user_role.ToList();
                    foreach (var entry in entries)
                    {
                        var userRole = new user_role
                        {
                            id = entry.UserRoleId,
                            code = entry.Code,
                            description = entry.Description
                        };
                        var exists = entityList.Any(x => x.id == entry.UserRoleId);
                        if (!exists)
                        {
                            entityList.Add(userRole);
                        }
                    }
                    newContext.user_role.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("user_role", "id", "user_role_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("user_role", "id", "user_role_id_seq");
                    SequenceHandler.SetSequenceForTable("user_role", "id", "user_role_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.user_role.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.user_role.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
