﻿using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_release_note_locale_exclusion : Migrator
    {
        public class ReleaseNoteLocaleExclusion
        {
            public int ReleaseNoteId { get; set; }
            public int ExcludeLocaleId { get; set; }
        }

        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.Database.SqlQuery<ReleaseNoteLocaleExclusion>("select * from ReleaseNoteLocaleExclusion", new object[0]).ToList();
                if (entries.Count > 0 && (newContext.release_note_locale_exclusion.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table release_note_locale_exclusion", new object[0]);
                    var entityList = newContext.release_note_locale_exclusion.ToList();
                    foreach (var entry in entries)
                    {
                        var releaseNoteLocaleExclusion = new release_note_locale_exclusion
                        {
                            release_note_id = entry.ReleaseNoteId,
                            excluded_locale_id = entry.ExcludeLocaleId
                        };
                        var exists = entityList.Any(x => 
                            x.release_note_id == entry.ReleaseNoteId && 
                            x.excluded_locale_id == entry.ExcludeLocaleId);
                        if (!exists)
                        {
                            entityList.Add(releaseNoteLocaleExclusion);
                        }
                    }
                    newContext.release_note_locale_exclusion.AddRange(entityList);
                    newContext.SaveChanges();
                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.release_note_locale_exclusion.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.release_note_locale_exclusion.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
