﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_release_strategy : Migrator
    {
        public class ReleaseNoteLocaleExclusion
        {
            public int ReleaseNoteId { get; set; }
            public int ExcludeLocaleId { get; set; }
        }

        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.ReleaseStrategies.ToList();
                if (entries.Count > 0 && (newContext.release_strategy.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table release_strategy", new object[0]);
                    SequenceHandler.DropSequenceForTable("release_strategy_id_seq");
                    
                    var entityList = newContext.release_strategy.ToList();
                    foreach (var entry in entries)
                    {
                        var releaseStrategy = new release_strategy
                        {
                            id = entry.ReleaseStrategyId,
                            code = entry.Code
                        };
                        var exists = entityList.Any(x => 
                            x.id == entry.ReleaseStrategyId && x.code == entry.Code);
                        if (!exists)
                        {
                            entityList.Add(releaseStrategy);
                        }
                    }
                    newContext.release_strategy.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("release_strategy", "id", "release_strategy_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("release_strategy", "id", "release_strategy_id_seq");
                    SequenceHandler.SetSequenceForTable("release_strategy", "id", "release_strategy_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.release_strategy.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.release_strategy.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
