﻿using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_program_ftp_mapping : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.ProgramFtpMappings.ToList();
                if (entries.Count > 0 && (newContext.program_ftp_mapping.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table program_ftp_mapping", new object[0]);

                    var entityList = newContext.program_ftp_mapping.ToList();
                    foreach (var entry in entries)
                    {
                        var programFtpMapping = new program_ftp_mapping
                        {
                            program_id = entry.ProgramId,
                            virtual_path = entry.VirtualPath,
                            real_path = entry.RealPath
                        };
                        var exists = entityList.Any(x => 
                            x.program_id == entry.ProgramId &&
                            x.virtual_path == entry.VirtualPath &&
                            x.real_path == entry.RealPath);
                        if (!exists)
                        {
                            entityList.Add(programFtpMapping);
                        }
                    }
                    newContext.program_ftp_mapping.AddRange(entityList);
                    newContext.SaveChanges();
                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.program_ftp_mapping.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.program_ftp_mapping.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
