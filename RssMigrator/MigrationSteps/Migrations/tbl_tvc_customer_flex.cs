﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_tvc_customer_flex : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entryCount = oldContext.tvc_customer_flex.Count();
                if (entryCount > 0 && (newContext.tvc_customer_flex.Count() < entryCount))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table tvc_customer_flex", new object[0]);
                    
                    var entries = oldContext.tvc_customer_flex.ToList();
                    var entityList = new List<tvc_customer_flex>();
                    foreach (var entry in entries)
                    {
                        var customerFlex = new tvc_customer_flex
                        {
                            id = entry.flexID,
                            administration_id = entry.administrationID,
                            customer_id = entry.customerID,
                            firmware_version = entry.firmwareVersion,
                            computer_name = entry.ComputerName,
                            serial_number = entry.SerialNumber,
                            first_report = entry.firstReport,
                            latest_activity = entry.latestActivity
                        };
                        var exists = entityList.Any(x => 
                            x.id == entry.flexID && 
                            x.administration_id == entry.administrationID && 
                            x.customer_id == entry.customerID &&
                            x.firmware_version == entry.firmwareVersion);
                        if (!exists)
                        {
                            entityList.Add(customerFlex);
                        }
                    }
                    newContext.tvc_customer_flex.AddRange(entityList);
                    newContext.SaveChanges();

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.tvc_customer_flex.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entryCount}/{newContext.tvc_customer_flex.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
