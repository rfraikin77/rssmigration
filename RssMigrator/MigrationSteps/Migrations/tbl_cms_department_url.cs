﻿using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_cms_department_url : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.cms_department_url.ToList();
                if (entries.Count > 0 && (newContext.cms_department_url.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table cms_department_url", new object[0]);
                    var entityList = newContext.cms_department_url.ToList();
                    foreach (var entry in entries)
                    {
                        var department_url = new cms_department_url
                        {
                            department_id = entry.departmentID,
                            url = entry.URL,
                            is_default = entry.@default
                        };
                        var exists = entityList.Any(x => x.url == entry.URL);
                        if (!exists)
                        {
                            entityList.Add(department_url);
                        }
                    }
                    newContext.cms_department_url.AddRange(entityList);
                    newContext.SaveChanges();
                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.cms_department_url.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.cms_department_url.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
