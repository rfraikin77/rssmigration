﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_tvc_customer_dos_flex : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.tvc_customer_dos_flex.ToList();
                if (entries.Count > 0 && (newContext.tvc_customer_dos_flex.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table tvc_customer_dos_flex", new object[0]);
                    SequenceHandler.DropSequenceForTable("tvc_customer_dos_flex_id_seq");

                    var index = 1;
                    var entityList = newContext.tvc_customer_dos_flex.ToList();
                    foreach (var entry in entries)
                    {
                        var customerDosFlex = new tvc_customer_dos_flex
                        {
                            id = index,
                            customer_id = entry.customerID,
                            scale1 = entry.scale1,
                            scale2 = entry.scale2,
                            scale3 = entry.scale3,
                            latest_activity = entry.latestActivity
                        };
                        var exists = entityList.Any(x => 
                            x.customer_id == entry.customerID &&
                            x.scale1 == entry.scale1 &&
                            x.scale2 == entry.scale2 &&
                            x.scale3 == entry.scale3 &&
                            x.latest_activity == entry.latestActivity);
                        if (!exists)
                        {
                            entityList.Add(customerDosFlex);
                        }
                        index++;
                    }
                    newContext.tvc_customer_dos_flex.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("tvc_customer_dos_flex", "id", "tvc_customer_dos_flex_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("tvc_customer_dos_flex", "id", "tvc_customer_dos_flex_id_seq");
                    SequenceHandler.SetSequenceForTable("tvc_customer_dos_flex", "id", "tvc_customer_dos_flex_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.tvc_customer_dos_flex.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.tvc_customer_dos_flex.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
