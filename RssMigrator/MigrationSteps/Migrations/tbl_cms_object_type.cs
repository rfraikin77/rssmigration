﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_cms_object_type : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.cms_object_type.ToList();
                if (entries.Count > 0 && (newContext.cms_object_type.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table cms_object_type", new object[0]);
                    SequenceHandler.DropSequenceForTable("cms_object_type_id_seq");

                    var entityList = newContext.cms_object_type.ToList();
                    foreach (var entry in entries)
                    {
                        var cmsObjectType = new cms_object_type
                        {
                            id = entry.typeId,
                            code = entry.code
                        };
                        var exists = entityList.Any(x => x.id == entry.typeId);
                        if (!exists)
                        {
                            entityList.Add(cmsObjectType);
                        }
                    }
                    newContext.cms_object_type.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("cms_object_type", "id", "cms_object_type_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("cms_object_type", "id", "cms_object_type_id_seq");
                    SequenceHandler.SetSequenceForTable("cms_object_type", "id", "cms_object_type_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.cms_object_type.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.cms_object_type.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
