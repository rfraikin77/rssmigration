﻿using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_cmsx_newsitem : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.cmsx_newsitem.ToList();
                if (entries.Count > 0 && (newContext.cmsx_newsitem.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table cmsx_newsitem", new object[0]);
                    var entityList = newContext.cmsx_newsitem.ToList();
                    foreach (var entry in entries)
                    {
                        var cmsXnewsItem = new cmsx_newsitem
                        {
                            revision_id = entry.revisionId,
                            news_title = entry.title,
                            teaser = entry.teaser,
                            body = entry.body,
                            publication_date = entry.publicationDate,
                            icon_url = entry.iconURL,
                            icon_width = entry.iconWidth,
                            icon_height = entry.iconHeight
                        };
                        var exists = entityList.Any(x => 
                            x.revision_id == entry.revisionId &&
                            x.news_title == entry.title &&
                            x.teaser == entry.teaser &&
                            x.body == entry.body &&
                            x.publication_date == entry.publicationDate &&
                            x.icon_url == entry.iconURL &&
                            x.icon_width == entry.iconWidth &&
                            x.icon_height == entry.iconHeight);
                        if (!exists)
                        {
                            entityList.Add(cmsXnewsItem);
                        }
                    }
                    newContext.cmsx_newsitem.AddRange(entityList);
                    newContext.SaveChanges();
                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.cmsx_newsitem.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.cmsx_newsitem.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
