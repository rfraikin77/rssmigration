﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_tp_contact_request : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.tp_contact_request.ToList();
                if (entries.Count > 0 && (newContext.tp_contact_request.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table tp_contact_request", new object[0]);
                    SequenceHandler.DropSequenceForTable("tp_contact_request_id_seq");
                    
                    var entityList = newContext.tp_contact_request.ToList();
                    foreach (var entry in entries)
                    {
                        var tpContactRequest = new tp_contact_request
                        {
                            id = entry.requestID,
                            name = entry.name,
                            company = entry.company,
                            address = entry.address,
                            postal_code = entry.postalcode,
                            city = entry.city,
                            phone = entry.phone,
                            email = entry.email,
                            type_id = entry.type,
                            category = entry.category,
                            question = entry.question,
                            website_id = entry.websiteID,
                            audit_date = entry.audit_date
                        };
                        var exists = entityList.Any(x => 
                            x.id == entry.requestID &&
                            x.name == entry.name &&
                            x.company == entry.company &&
                            x.address == entry.address &&
                            x.postal_code == entry.postalcode &&
                            x.city == entry.city &&
                            x.phone == entry.phone &&
                            x.email == entry.email &&
                            x.type_id == entry.type &&
                            x.category == entry.category &&
                            x.question == entry.question &&
                            x.website_id == entry.websiteID &&
                            x.audit_date == entry.audit_date);
                        if (!exists)
                        {
                            entityList.Add(tpContactRequest);
                        }
                    }
                    newContext.tp_contact_request.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("tp_contact_request", "id", "tp_contact_request_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("tp_contact_request", "id", "tp_contact_request_id_seq");
                    SequenceHandler.SetSequenceForTable("tp_contact_request", "id", "tp_contact_request_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.tp_contact_request.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.tp_contact_request.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
