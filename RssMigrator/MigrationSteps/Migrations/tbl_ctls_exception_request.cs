﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_ctls_exception_request : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entryCount = oldContext.ctls_exception_request.Count();
                if (entryCount > 0 && newContext.ctls_exception_request.Count() < entryCount)
                {
                    newContext.Database.ExecuteSqlCommand("truncate table ctls_exception_request", new object[0]);
                    SequenceHandler.DropSequenceForTable("ctls_exception_request_id_seq");

                    var entries = oldContext.ctls_exception_request.OrderBy(x => x.requestID).ToList();
                    var entityList = new List<ctls_exception_request>();
                    foreach (var entry in entries)
                    {
                        var ctlsExceptionRequest = new ctls_exception_request
                        {
                            id = entry.requestID,
                            exception_id = entry.exceptionID,
                            timestamp = entry.timestamp,
                            script_name = entry.scriptName,
                            query_string = entry.queryString,
                            referer = entry.referer,
                            post = entry.post ? 1 : 0,
                            data = entry.data,
                        };
                        entityList.Add(ctlsExceptionRequest);
                    }
                    newContext.ctls_exception_request.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("ctls_exception_request", "id", "ctls_exception_request_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("ctls_exception_request", "id", "ctls_exception_request_id_seq");
                    SequenceHandler.SetSequenceForTable("ctls_exception_request", "id", "ctls_exception_request_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.ctls_exception_request.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entryCount}/{newContext.ctls_exception_request.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
