﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_t_customer : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.t_customer.ToList();
                if (entries.Count > 0 && (newContext.t_customer.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table t_customer", new object[0]);
                    SequenceHandler.DropSequenceForTable("t_customer_id_seq");
                    
                    var entityList = newContext.t_customer.ToList();
                    foreach (var entry in entries)
                    {
                        var customer = new t_customer
                        {
                            id = entry.customerID,
                            group_id = entry.groupID,
                            reflex_id = entry.ReflexID,
                            name = entry.name,
                            name_index = entry.nameIndex,
                            address = entry.address,
                            postal_code = entry.postalCode,
                            city = entry.city,
                            locale_id = entry.localeID,
                            debtor = entry.debtor ? 1 : 0,
                            dos = entry.DOS ? 1 : 0,
                            bookkeeping_software_id = entry.bookkeepingSoftwareID,
                            reflex_license = entry.ReflexLicense,
                            licensed_users = entry.licensedUsers,
                            has_support_contract = entry.HasSupportContract ? 1 : 0,
                            has_update_contract = entry.HasUpdateContract ? 1 : 0,
                            date_created = entry.dateCreated,
                            date_modified = entry.dateModified,
                            deleted = entry.deleted ? 1 : 0,
                            is_transito_customer = entry.IsTransitoCustomer ? 1 : 0,
                            customer_group_id = entry.CustomerGroupId,
                            security_level = entry.SecurityLevel,
                            latitude = entry.Latitude,
                            longitude = entry.Longitude,
                            download_expiration_date = entry.DownloadExpirationDate,
                            can_receive_notifications = entry.CanReceiveNotifications,
                            can_download = entry.CanDownload ? 1 : 0,
                            can_upload = entry.CanUpload ? 1 : 0,
                            upload_expiration_date = entry.UploadExpirationDate,
                            is_on_credit_hold = entry.IsOnCreditHold ? 1 : 0,
                            reflex_blue_id = null
                        };
                        var exists = entityList.Any(x => x.id == entry.customerID);
                        if (!exists)
                        {
                            entityList.Add(customer);
                        }
                    }
                    newContext.t_customer.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("t_customer", "id", "t_customer_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("t_customer", "id", "t_customer_id_seq");
                    SequenceHandler.SetSequenceForTable("t_customer", "id", "t_customer_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.t_customer.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.t_customer.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
