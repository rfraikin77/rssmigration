﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_tvc_release_note : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.tvc_release_note.ToList();
                if (entries.Count > 0 && (newContext.tvc_release_note.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table tvc_release_note", new object[0]);
                    newContext.Database.ExecuteSqlCommand("ALTER TABLE tvc_release_note ALTER COLUMN id DROP IDENTITY", new object[0]);
                    SequenceHandler.DropSequenceForTable("tvc_release_note_id_seq");
                    
                    var entityList = newContext.tvc_release_note.ToList();
                    foreach (var entry in entries)
                    {
                        var releaseNote = new tvc_release_note
                        {
                            id = entry.noteID,
                            type_id = entry.typeID,
                            @internal = entry.@internal ? 1 : 0,
                            solution_code = entry.solutionCode,
                            deleted = entry.deleted ? 1 : 0,
                            customer_exclusive = entry.customerExclusive ? 1 : 0,
                            status_id = entry.statusID,
                            source_id = entry.sourceID,
                            time_spent = entry.timespent,
                            priority = entry.priority,
                            approved = entry.approved ? 1 : 0,
                            audit_date_created = entry.audit_dateCreated,
                            audit_user_id_created = entry.audit_userIDCreated,
                            order_id = entry.orderID,
                            lower_notify_build_id = entry.lowerNotifyBuildID,
                            docs_status_id = entry.docsStatusID,
                            impact_id = entry.impactID,
                            notification_status_id = entry.notificationStatusID,
                            risk_id = entry.riskID,
                            cause_id = entry.CauseId,
                            test_description = entry.TestDescription,
                            include_non_public_releases_in_notification = entry.IncludeNonPublicReleasesInNotification ? 1 : 0,
                            version_program_id = entry.VersionProgramId,
                            pivotal_story_id = entry.PivotalStoryId
                        };
                        var exists = entityList.Any(x => x.id == entry.noteID);
                        if (!exists)
                        {
                            entityList.Add(releaseNote);
                        }
                    }
                    newContext.tvc_release_note.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("tvc_release_note", "id", "tvc_release_note_id_seq");
                    newContext.Database.ExecuteSqlCommand("ALTER TABLE tvc_release_note ALTER COLUMN id set DEFAULT nextval('tvc_release_note_id_seq')", new object[0]);
                    SequenceHandler.SetSequenceForTable("tvc_release_note", "id", "tvc_release_note_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.tvc_release_note.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.tvc_release_note.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
