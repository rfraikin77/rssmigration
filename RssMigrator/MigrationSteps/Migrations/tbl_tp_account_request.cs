﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_tp_account_request : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.tp_account_request.ToList();
                if (entries.Count > 0 && (newContext.tp_account_request.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table tp_account_request", new object[0]);
                    SequenceHandler.DropSequenceForTable("tp_account_request_id_seq");

                    var entityList = newContext.tp_account_request.ToList();
                    foreach (var entry in entries)
                    {
                        var tpAccountRequest = new tp_account_request
                        {
                            id = entry.requestID,
                            customer_id = entry.customerID,
                            reflex_license = entry.ReflexLicense,
                            username = entry.user_name,
                            customer_name = entry.customer_name,
                            email = entry.email,
                            audit_date_created = entry.audit_date_created,
                            audit_date_identified = entry.audit_date_identified,
                            deleted = entry.deleted ? 1 : 0
                        };
                        var exists = entityList.Any(x => 
                            x.id == entry.requestID &&
                            x.customer_id == entry.customerID &&
                            x.reflex_license == entry.ReflexLicense &&
                            x.username == entry.user_name &&
                            x.customer_name == entry.customer_name &&
                            x.email == entry.email &&
                            x.audit_date_created == entry.audit_date_created &&
                            x.audit_date_identified == entry.audit_date_identified);
                        if (!exists)
                        {
                            entityList.Add(tpAccountRequest);
                        }
                    }
                    newContext.tp_account_request.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("tp_account_request", "id", "tp_account_request_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("tp_account_request", "id", "tp_account_request_id_seq");
                    SequenceHandler.SetSequenceForTable("tp_account_request", "id", "tp_account_request_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.tp_account_request.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.tp_account_request.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
