﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_tvc_release_note_call : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.tvc_release_note_call.ToList();
                if (entries.Count > 0 && (newContext.tvc_release_note_call.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table tvc_release_note_call", new object[0]);
                    SequenceHandler.DropSequenceForTable("tvc_release_note_call_id_seq");

                    var index = 1;
                    var entityList = newContext.tvc_release_note_call.ToList();
                    foreach (var entry in entries)
                    {
                        var releaseNoteCall = new tvc_release_note_call
                        {
                            id = index,
                            note_id = entry.noteID,
                            call_id = entry.callID
                        };
                        var exists = entityList.Any(x => x.note_id == entry.noteID && x.call_id == entry.callID);
                        if (!exists)
                        {
                            entityList.Add(releaseNoteCall);
                        }
                        index++;
                    }
                    newContext.tvc_release_note_call.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("tvc_release_note_call", "id", "tvc_release_note_call_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("tvc_release_note_call", "id", "tvc_release_note_call_id_seq");
                    SequenceHandler.SetSequenceForTable("tvc_release_note_call", "id", "tvc_release_note_call_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.tvc_release_note_call.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.tvc_release_note_call.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
