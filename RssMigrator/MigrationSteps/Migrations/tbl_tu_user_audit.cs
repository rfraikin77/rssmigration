﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_tu_user_audit : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.tu_user_audit.Count();
                if (entries > 0 && (newContext.tu_user_audit.Count() < entries))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table tu_user_audit", new object[0]);
                    SequenceHandler.DropSequenceForTable("tu_user_audit_id_seq");

                    var index = 1;
                    var entityList = new System.Collections.Generic.List<tu_user_audit>();
                    var oldList = oldContext.tu_user_audit.ToList();
                    foreach (var entry in oldList)
                    {
                        var userAudit = new tu_user_audit
                        {
                            id = index,
                            user_id = entry.userID,
                            audit_user_id = entry.audit_userID,
                            audit_date = entry.audit_date,
                            audit_action = entry.audit_action
                        };
                        entityList.Add(userAudit);
                        index++;
                    }
                    newContext.tu_user_audit.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("tu_user_audit", "id", "tu_user_audit_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("tu_user_audit", "id", "tu_user_audit_id_seq");
                    SequenceHandler.SetSequenceForTable("tu_user_audit", "id", "tu_user_audit_id_seq");

                    TableMigrationJournal = $" (R: {entries} / W: {newContext.tu_user_audit.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries}/{newContext.tu_user_audit.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
