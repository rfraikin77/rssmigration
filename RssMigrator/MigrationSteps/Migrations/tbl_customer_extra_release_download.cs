﻿using RssMigrator.NewContext;
using System;
using System.Linq;
using RssMigrator.MigrationSteps.Functions;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_customer_extra_release_download : Migrator
    {
        public class ExtraReleaseDownload
        {
            public int CustomerId { get; set; }
            public int ReleaseId { get; set; }
        }

        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.Database.SqlQuery<ExtraReleaseDownload>("select * from CustomerExtraReleaseDownload", new object[0]).ToList();
                if (entries.Count > 0 && (newContext.customer_extra_release_download.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table customer_extra_release_download", new object[0]);

                    foreach (var entry in entries)
                    {
                        var customerExtraReleaseDownload = new customer_extra_release_download
                        {
                            customer_id = entry.CustomerId,
                            release_id = entry.ReleaseId
                        };
                        var exists = newContext.customer_extra_release_download.Any(x => x.customer_id == entry.CustomerId);
                        if (!exists)
                        {
                            newContext.customer_extra_release_download.Add(customerExtraReleaseDownload);
                            newContext.SaveChanges();
                        }
                    }
                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.customer_extra_release_download.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.customer_extra_release_download.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
