﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_t_user_phone_group : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.t_user_phone_group.ToList();
                if (entries.Count > 0 && (newContext.t_user_phone_group.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table t_user_phone_group", new object[0]);
                    SequenceHandler.DropSequenceForTable("t_user_phone_group_id_seq");
                    
                    var entityList = newContext.t_user_phone_group.ToList();
                    foreach (var entry in entries)
                    {
                        var userPhoneGroup = new t_user_phone_group
                        {
                            id = entry.phoneGroupID,
                            name = entry.name
                        };
                        var exists = entityList.Any(x => x.id == entry.phoneGroupID && x.name == entry.name);
                        if (!exists)
                        {
                            entityList.Add(userPhoneGroup);
                        }
                    }
                    newContext.t_user_phone_group.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("t_user_phone_group", "id", "t_user_phone_group_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("t_user_phone_group", "id", "t_user_phone_group_id_seq");
                    SequenceHandler.SetSequenceForTable("t_user_phone_group", "id", "t_user_phone_group_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.t_user_phone_group.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.t_user_phone_group.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
