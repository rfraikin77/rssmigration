﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_module_release_block : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.ModuleReleaseBlocks.ToList();
                if (entries.Count > 0 && (newContext.module_release_block.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table module_release_block cascade", new object[0]);
                    SequenceHandler.DropSequenceForTable("module_release_block_id_seq");

                    foreach (var entry in entries)
                    {
                        var moduleReleaseBlock = new module_release_block
                        {
                            id = entry.ModuleReleaseBlockId,
                            blocked_from_release_id = entry.BlockedFromReleaseId,
                            blocked_until_release_id = entry.BlockedUntilReleaseId,
                            top_rule_id = entry.TopRuleId,
                            reason = entry.Reason,
                            blocked_on = entry.BlockedOn,
                            blocked_by = entry.BlockedBy
                        };
                        var exists = newContext.module_release_block.Any(x => x.id == entry.ModuleReleaseBlockId);
                        if (!exists)
                        {
                            newContext.module_release_block.Add(moduleReleaseBlock);
                            newContext.SaveChanges();
                        }
                    }

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("module_release_block", "id", "module_release_block_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("module_release_block", "id", "module_release_block_id_seq");
                    SequenceHandler.SetSequenceForTable("module_release_block", "id", "module_release_block_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.module_release_block.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.module_release_block.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
