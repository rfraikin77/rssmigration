﻿using RssMigrator.NewContext;
using System;
using System.Linq;
using RssMigrator.MigrationSteps.Functions;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_tvc_customer_build : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entryCount = oldContext.tvc_customer_build.Count();
                if (entryCount > 0 && (newContext.tvc_customer_build.Count() < entryCount))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table tvc_customer_build", new object[0]);
                    
                    var entries = oldContext.tvc_customer_build.ToList();
                    var entityList = newContext.tvc_customer_build.ToList();
                    foreach (var entry in entries)
                    {
                        var customerBuild = new tvc_customer_build
                        {
                            customer_id = entry.customerID,
                            build_id = entry.buildID,
                            date_reported = entry.dateReported,
                            origin = entry.origin
                        };
                        var exists = entityList.Any(x => x.customer_id == entry.customerID && x.build_id == entry.buildID && x.origin == entry.origin);
                        if (!exists)
                        {
                            entityList.Add(customerBuild);
                        }
                    }
                    newContext.tvc_customer_build.AddRange(entityList);
                    newContext.SaveChanges();
                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.tvc_customer_build.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entryCount}/{newContext.tvc_customer_build.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
