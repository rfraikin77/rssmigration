﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_tvcs_release_note_impact : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.tvcs_release_note_impact.ToList();
                if (entries.Count > 0 && (newContext.tvcs_release_note_impact.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table tvcs_release_note_impact", new object[0]);
                    SequenceHandler.DropSequenceForTable("tvcs_release_note_impact_id_seq");
                    
                    var entityList = newContext.tvcs_release_note_impact.ToList();
                    foreach (var entry in entries)
                    {
                        var releaseNoteImpact = new tvcs_release_note_impact
                        {
                            id = entry.impactID,
                            code = entry.code
                        };
                        var exists = entityList.Any(x => x.id == entry.impactID);
                        if (!exists)
                        {
                            entityList.Add(releaseNoteImpact);
                        }
                    }
                    newContext.tvcs_release_note_impact.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("tvcs_release_note_impact", "id", "tvcs_release_note_impact_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("tvcs_release_note_impact", "id", "tvcs_release_note_impact_id_seq");
                    SequenceHandler.SetSequenceForTable("tvcs_release_note_impact", "id", "tvcs_release_note_impact_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.tvcs_release_note_impact.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.tvcs_release_note_impact.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
