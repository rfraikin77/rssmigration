﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_tvc_release_note_comment : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.tvc_release_note_comment.ToList();
                if (entries.Count > 0 && (newContext.tvc_release_note_comment.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table tvc_release_note_comment", new object[0]);
                    SequenceHandler.DropSequenceForTable("tvc_release_note_comment_id_seq");
                    
                    var entityList = newContext.tvc_release_note_comment.ToList();
                    foreach (var entry in entries)
                    {
                        var releaseNoteComment = new tvc_release_note_comment
                        {
                            id = entry.commentID,
                            note_id = entry.noteID,
                            user_id = entry.userID,
                            audit_date_created = entry.audit_date_created,
                            audit_date_modified = entry.audit_date_modified,
                            content_text = entry.text,
                            deleted = entry.deleted ? 1 : 0
                        };
                        var exists = entityList.Any(x => 
                            x.id == entry.commentID &&
                            x.note_id == entry.noteID &&
                            x.user_id == entry.userID &&
                            x.audit_date_created == entry.audit_date_created &&
                            x.audit_date_modified == entry.audit_date_modified &&
                            x.content_text == entry.text &&
                            x.deleted == (entry.deleted ? 1 : 0));
                        if (!exists)
                        {
                            entityList.Add(releaseNoteComment);
                        }
                    }
                    newContext.tvc_release_note_comment.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("tvc_release_note_comment", "id", "tvc_release_note_comment_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("tvc_release_note_comment", "id", "tvc_release_note_comment_id_seq");
                    SequenceHandler.SetSequenceForTable("tvc_release_note_comment", "id", "tvc_release_note_comment_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.tvc_release_note_comment.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.tvc_release_note_comment.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
