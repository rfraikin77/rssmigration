﻿using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_customer_licensed_program : Migrator
    {
        public class CustomerLicensedProgram
        {
            public int CustomerId { get; set; }
            public int ProgramId { get; set; }
        }

        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.Database.SqlQuery<CustomerLicensedProgram>("select * from CustomerLicensedProgram", new object[0]).ToList();
                if (entries.Count > 0 && (newContext.customer_licensed_program.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table customer_licensed_program", new object[0]);

                    foreach (var entry in entries)
                    {
                        var customerLicensedProgram = new customer_licensed_program
                        {
                            customer_id = entry.CustomerId,
                            program_id = entry.ProgramId
                        };
                        var exists = newContext.customer_licensed_program.Any(x => x.customer_id == entry.CustomerId && x.program_id == entry.ProgramId);
                        if (!exists)
                        {
                            newContext.customer_licensed_program.Add(customerLicensedProgram);
                            newContext.SaveChanges();
                        }
                    }
                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.customer_licensed_program.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.customer_licensed_program.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
