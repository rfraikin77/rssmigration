﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_tvcs_module : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.tvcs_module.ToList();
                if (entries.Count > 0 && (newContext.tvcs_module.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table tvcs_module", new object[0]);
                    SequenceHandler.DropSequenceForTable("tvcs_module_id_seq");
                    
                    var entityList = newContext.tvcs_module.ToList();
                    foreach (var entry in entries)
                    {
                        var tvcsModule = new tvcs_module
                        {
                            id = entry.moduleID,
                            program_id = entry.programID,
                            parent_id = entry.parentID,
                            name = entry.name,
                            @internal = entry.@internal ? 1 : 0,
                            deleted = entry.deleted ? 1 : 0,
                            reflex_module_id = entry.ReflexModuleId,
                            reflex_module_name = entry.ReflexModuleName,
                            sort_index = entry.SortIndex
                        };
                        var exists = entityList.Any(x => 
                            x.id == entry.moduleID &&
                            x.parent_id == entry.parentID &&
                            x.program_id == entry.programID &&
                            x.reflex_module_id == entry.ReflexModuleId &&
                            x.sort_index == entry.SortIndex);
                        if (!exists)
                        {
                            entityList.Add(tvcsModule);
                        }
                    }
                    newContext.tvcs_module.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("tvcs_module", "id", "tvcs_module_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("tvcs_module", "id", "tvcs_module_id_seq");
                    SequenceHandler.SetSequenceForTable("tvcs_module", "id", "tvcs_module_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.tvcs_module.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.tvcs_module.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
