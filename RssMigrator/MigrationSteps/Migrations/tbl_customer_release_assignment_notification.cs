﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_customer_release_assignment_notification : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.CustomerReleaseAssignmentNotifications.ToList();
                if (entries.Count > 0 && (newContext.customer_release_assignment_notification.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table customer_release_assignment_notification", new object[0]);
                    SequenceHandler.DropSequenceForTable("customer_release_assignment_notification_id_seq");

                    var entityList = newContext.customer_release_assignment_notification.ToList();
                    foreach (var entry in entries)
                    {
                        var customerReleaseAssignmentNotification = new customer_release_assignment_notification
                        {
                            id = entry.CustomerReleaseAssignmentId,
                            user_id = entry.UserId,
                            date_created = entry.DateCreated,
                            email_notified = entry.EmailNotified,
                            date_notified = entry.DateNotified,
                            emailing_disabled = entry.EmailingDisabled ? 1 : 0
                        };
                        var exists = entityList.Any(x => x.id == entry.CustomerReleaseAssignmentId && x.user_id == entry.UserId);
                        if (!exists)
                        {
                            entityList.Add(customerReleaseAssignmentNotification);
                        }
                    }
                    newContext.customer_release_assignment_notification.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("customer_release_assignment_notification", "id", "customer_release_assignment_notification_id_seq");
                    SequenceHandler.SetSequenceForTable("customer_release_assignment_notification", "id", "customer_release_assignment_notification_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.customer_release_assignment_notification.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.customer_release_assignment_notification.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
