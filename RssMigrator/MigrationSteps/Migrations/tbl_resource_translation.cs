﻿using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_resource_translation : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.ResourceTranslations.ToList();
                if (entries.Count > 0 && (newContext.resource_translation.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table resource_translation", new object[0]);
                    var entityList = newContext.resource_translation.ToList();
                    foreach (var entry in entries)
                    {
                        var resourceTranslation = new resource_translation
                        {
                            resource_code_id = entry.ResourceCodeId,
                            locale_id = entry.LocaleId,
                            translation_content = entry.Translation,
                            date_last_modified = entry.DateLastModified
                        };
                        var exists = entityList.Any(x => 
                            x.resource_code_id == entry.ResourceCodeId && 
                            x.locale_id == entry.LocaleId &&
                            x.translation_content == entry.Translation &&
                            x.date_last_modified == entry.DateLastModified);
                        if (!exists)
                        {
                            entityList.Add(resourceTranslation);
                        }
                    }
                    newContext.resource_translation.AddRange(entityList);
                    newContext.SaveChanges();
                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.resource_translation.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.resource_translation.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
