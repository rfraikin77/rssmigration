﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_cms_revision_live_audit : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.cms_revision_live_audit.ToList();
                if (entries.Count > 0 && (newContext.cms_revision_live_audit.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table cms_revision_live_audit", new object[0]);
                    SequenceHandler.DropSequenceForTable("cms_revision_live_audit_id_seq");
                    SequenceHandler.DropSequenceForTable("cms_revision_live_audit_object_id_seq");

                    var entityList = newContext.cms_revision_live_audit.ToList();
                    foreach (var entry in entries)
                    {
                        var cmsRevisionLiveAudit = new cms_revision_live_audit
                        {
                            id = entry.auditId,
                            object_id = entry.objectId,
                            revision_id = entry.revisionId,
                            audit_user_id = entry.audit_userId,
                            audit_date = entry.audit_date,
                        };
                        var exists = entityList.Any(x => 
                            x.id == entry.auditId &&
                            x.object_id == entry.objectId &&
                            x.revision_id == entry.revisionId &&
                            x.audit_user_id == entry.audit_userId &&
                            x.audit_date == entry.audit_date);
                        if (!exists)
                        {
                            entityList.Add(cmsRevisionLiveAudit);
                        }
                    }
                    newContext.cms_revision_live_audit.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("cms_revision_live_audit", "id", "cms_revision_live_audit_id_seq");
                    SequenceHandler.CreateSequenceForTable("cms_revision_live_audit", "object_id", "cms_revision_live_audit_object_id_seq");
                    SequenceHandler.SetSequenceForTable("cms_revision_live_audit", "id", "cms_revision_live_audit_id_seq");
                    SequenceHandler.SetSequenceForTable("cms_revision_live_audit", "object_id", "cms_revision_live_audit_object_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.cms_revision_live_audit.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.cms_revision_live_audit.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
