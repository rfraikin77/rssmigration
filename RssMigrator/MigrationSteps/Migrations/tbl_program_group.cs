﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_program_group : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.ProgramGroups.ToList();
                if (entries.Count > 0 && (newContext.program_group.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table program_group cascade", new object[0]);
                    SequenceHandler.DropSequenceForTable("program_group_id_seq");
                    
                    var entityList = newContext.program_group.ToList();
                    foreach (var entry in entries)
                    {
                        var programGroup = new program_group
                        {
                            id = entry.ProgramGroupId,
                            code = entry.Code
                        };
                        var exists = entityList.Any(x => x.id == entry.ProgramGroupId && x.code == entry.Code);
                        if (!exists)
                        {
                            entityList.Add(programGroup);
                        }
                    }
                    newContext.program_group.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("program_group", "id", "program_group_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("program_group", "id", "program_group_id_seq");
                    SequenceHandler.SetSequenceForTable("program_group", "id", "program_group_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.program_group.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.program_group.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
