﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System.Collections.Generic;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_ctls_exception : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            var entryCount = oldContext.ctls_exception.Count();
            if (entryCount > 0 && newContext.ctls_exception.Count() < entryCount)
            {
                newContext.Database.ExecuteSqlCommand("truncate table ctls_exception", new object[0]);
                SequenceHandler.DropSequenceForTable("ctls_exception_id_seq");

                var entries = oldContext.ctls_exception.OrderBy(x => x.exceptionID).ToList();
                var entityList = new List<ctls_exception>();
                foreach (var entry in entries)
                {
                    var ctlsException = new ctls_exception
                    {
                        id = entry.exceptionID,
                        host = entry.host,
                        user_agent = entry.userAgent,
                        remote_addr = entry.remoteAddr,
                        cookie = entry.cookie,
                        exception_message = entry.message,
                        details = entry.detail,
                        stacktrace = entry.stacktrace,
                        data = entry.data,
                        deleted = entry.deleted ? 1 : 0,
                        date_created = entry.dateCreated
                    };
                    entityList.Add(ctlsException);
                }
                newContext.ctls_exception.AddRange(entityList);
                newContext.SaveChanges();

                //Update table sequence
                SequenceHandler.CreateSequenceForTable("ctls_exception", "id", "ctls_exception_id_seq");
                SequenceHandler.SetSequenceOnPkColumn("ctls_exception", "id", "ctls_exception_id_seq");
                SequenceHandler.SetSequenceForTable("ctls_exception", "id", "ctls_exception_id_seq");

                TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.ctls_exception.ToList().Count}) OK!";
                return TableMigrationJournal;
            }
            return $"[SKIPPED! ({entryCount}/{newContext.ctls_exception.ToList().Count})]";
        }
    }
}
