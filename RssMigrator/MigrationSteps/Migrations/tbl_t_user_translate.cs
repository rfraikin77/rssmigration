﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_t_user_translate : Migrator
    {
        public class UserTranslate
        {
            public int UserId { get; set; }
            public int LocaleId { get; set; }
        }

        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.Database.SqlQuery<UserTranslate>("select * from t_user_translate", new object[0]).ToList();
                if (entries.Count > 0 && (newContext.t_user_translate.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table t_user_translate", new object[0]);
                    SequenceHandler.DropSequenceForTable("t_user_translate_id_seq");

                    var index = 1;
                    var entityList = newContext.t_user_translate.ToList();
                    foreach (var entry in entries)
                    {
                        var userTranslate = new t_user_translate
                        {
                            id = index,
                            user_id = entry.UserId,
                            locale_id = entry.LocaleId
                        };
                        var exists = entityList.Any(x => x.user_id == entry.UserId && x.locale_id == entry.LocaleId);
                        if (!exists)
                        {
                            entityList.Add(userTranslate);
                        }
                        index++;
                    }
                    newContext.t_user_translate.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("t_user_translate", "id", "t_user_translate_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("t_user_translate", "id", "t_user_translate_id_seq");
                    SequenceHandler.SetSequenceForTable("t_user_translate", "id", "t_user_translate_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.t_user_translate.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.t_user_translate.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
