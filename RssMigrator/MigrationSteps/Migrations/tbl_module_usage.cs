﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_module_usage : Migrator
    {
        public class ModuleUsage
        {
            public int CustomerId { get; set; }
            public int ModuleId { get; set; }
        }

        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.Database.SqlQuery<ModuleUsage>("select * from ModuleUsage", new object[0]).ToList();
                if (entries.Count > 0 && (newContext.module_usage.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table module_usage", new object[0]);
                    SequenceHandler.DropSequenceForTable("module_usage_id_seq");

                    var index = 1;
                    var entityList = newContext.module_usage.ToList();
                    foreach (var entry in entries)
                    {
                        var moduleUsage = new module_usage
                        {
                            id = index,
                            customer_id = entry.CustomerId,
                            module_id = entry.ModuleId
                        };
                        var exists = entityList.Any(x => x.customer_id == entry.CustomerId && x.module_id == entry.ModuleId);
                        if (!exists)
                        {
                            entityList.Add(moduleUsage);
                        }
                        index++;
                    }
                    newContext.module_usage.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("module_usage", "id", "module_usage_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("module_usage", "id", "module_usage_id_seq");
                    SequenceHandler.SetSequenceForTable("module_usage", "id", "module_usage_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.module_usage.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.module_usage.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
