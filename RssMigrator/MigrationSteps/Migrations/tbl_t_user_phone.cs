﻿using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_t_user_phone : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.t_user_phone.ToList();
                if (entries.Count > 0 && (newContext.t_user_phone.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table t_user_phone", new object[0]);
                    var entityList = newContext.t_user_phone.ToList();
                    foreach (var entry in entries)
                    {
                        var userPhone = new t_user_phone
                        {
                            user_id = entry.userID,
                            phone_group_id = entry.phoneGroupID,
                            @internal = entry.@internal,
                            mobile = entry.mobile,
                            mobile_private = entry.mobilePrivate
                        };
                        var exists = entityList.Any(x => 
                            x.user_id == entry.userID && 
                            x.phone_group_id == entry.phoneGroupID &&
                            x.@internal == entry.@internal &&
                            x.mobile == entry.mobile &&
                            x.mobile_private == entry.mobilePrivate);
                        if (!exists)
                        {
                            entityList.Add(userPhone);
                        }
                    }
                    newContext.t_user_phone.AddRange(entityList);
                    newContext.SaveChanges();
                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.t_user_phone.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.t_user_phone.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
