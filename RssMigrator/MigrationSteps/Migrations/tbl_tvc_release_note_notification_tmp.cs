﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_tvc_release_note_notification_tmp : Migrator
    {
        public class ReleaseNoteNotificationTmp
        {
            public int? noteID { get; set; }
            public int? customerID { get; set; }
            public int? userID { get; set; }
            public DateTime? DateCreated { get; set; }
            public DateTime? DateNotified { get; set; }
            public string EmailNotified { get; set; }
            public bool? Disabled { get; set; }
            public bool? IsEmailValid { get; set; }
            public int? FileErrorReportId { get; set; }
        }

        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.Database.SqlQuery<ReleaseNoteNotificationTmp>("select * from tvc_release_note_notification_tmp", new object[0]).ToList();
                if (entries.Count > 0 && (newContext.tvc_release_note_notification_tmp.Count() < entries.Count))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table tvc_release_note_notification_tmp", new object[0]);
                    SequenceHandler.DropSequenceForTable("tvc_release_note_notification_tmp_id_seq");

                    var index = 1;
                    var entityList = newContext.tvc_release_note_notification_tmp.ToList();
                    foreach (var entry in entries)
                    {
                        var releaseNoteNotificationTmp = new tvc_release_note_notification_tmp
                        {
                            id = index,
                            note_id = entry.noteID,
                            customer_id = entry.customerID,
                            user_id = entry.userID,
                            date_created = entry.DateCreated,
                            date_notified = entry.DateNotified,
                            email_notified = entry.EmailNotified,
                            disabled = (bool)entry.Disabled ? 1 : 0,
                            file_error_report_id = entry.FileErrorReportId,
                            is_email_valid = (bool)entry.IsEmailValid ? 1 : 0
                        };
                        var exists = entityList.Any(x => 
                            x.note_id == entry.noteID &&
                            x.customer_id == entry.customerID &&
                            x.user_id == entry.userID &&
                            x.date_created == entry.DateCreated &&
                            x.date_notified == entry.DateNotified &&
                            x.email_notified == entry.EmailNotified &&
                            x.disabled == ((bool)entry.Disabled ? 1 : 0) &&
                            x.file_error_report_id == entry.FileErrorReportId &&
                            x.is_email_valid == ((bool)entry.IsEmailValid ? 1 : 0));
                        if (!exists)
                        {
                            entityList.Add(releaseNoteNotificationTmp);
                        }
                        index++;
                    }
                    newContext.tvc_release_note_notification_tmp.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("tvc_release_note_notification_tmp", "id", "tvc_release_note_notification_tmp_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("tvc_release_note_notification_tmp", "id", "tvc_release_note_notification_tmp_id_seq");
                    SequenceHandler.SetSequenceForTable("tvc_release_note_notification_tmp", "id", "tvc_release_note_notification_tmp_id_seq");

                    TableMigrationJournal = $" (R: {entries.Count} / W: {newContext.tvc_release_note_notification_tmp.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries.Count}/{newContext.tvc_release_note_notification_tmp.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
