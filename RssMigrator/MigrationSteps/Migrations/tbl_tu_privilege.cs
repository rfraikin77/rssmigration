﻿using RssMigrator.MigrationSteps.Functions;
using RssMigrator.NewContext;
using System;
using System.Linq;

namespace RssMigrator.MigrationSteps.Migrations
{
    public class tbl_tu_privilege : Migrator
    {
        public string Transfer()
        {
            oldContext = new OldContext.OldEntities();
            newContext = new NewEntities();

            try
            {
                var entries = oldContext.tu_privilege.Count();
                if (entries > 0 && (newContext.tu_privilege.Count() < entries))
                {
                    newContext.Database.ExecuteSqlCommand("truncate table tu_privilege", new object[0]);
                    SequenceHandler.DropSequenceForTable("tu_privilege_id_seq");
                    
                    var entityList = newContext.tu_privilege.ToList();
                    foreach (var entry in oldContext.tu_privilege.ToList())
                    {
                        var privilege = new tu_privilege
                        {
                            id = entry.privilegeID,
                            code = entry.code,
                            sort_index = entry.SortIndex
                        };
                        var exists = entityList.Any(x => x.id == entry.privilegeID);
                        if (!exists)
                        {
                            entityList.Add(privilege);
                        }
                    }
                    newContext.tu_privilege.AddRange(entityList);
                    newContext.SaveChanges();

                    //Update table sequence
                    SequenceHandler.CreateSequenceForTable("tu_privilege", "id", "tu_privilege_id_seq");
                    SequenceHandler.SetSequenceOnPkColumn("tu_privilege", "id", "tu_privilege_id_seq");
                    SequenceHandler.SetSequenceForTable("tu_privilege", "id", "tu_privilege_id_seq");

                    TableMigrationJournal = $" (R: {entries} / W: {newContext.tu_privilege.ToList().Count}) OK!";
                    return TableMigrationJournal;
                }
                return $"[SKIPPED! ({entries}/{newContext.tu_privilege.ToList().Count})]";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
