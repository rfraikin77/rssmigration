﻿using RssMigrator.NewContext;
using System.Linq;

namespace RssMigrator.MigrationSteps.Functions
{
    public static class SequenceHandler
    {
        private static readonly NewEntities NewContext = new NewEntities();

        public static void DropSequenceForTable(string sequenceName)
        {
            NewContext.Database.ExecuteSqlCommand("DROP SEQUENCE IF EXISTS " + sequenceName + " CASCADE;", new object[0]);
        }

        public static void CreateSequenceForTable(string tableName, string primaryKey, string sequenceName)
        {
            NewContext.Database.ExecuteSqlCommand("CREATE SEQUENCE " + sequenceName + " INCREMENT 1 START 1 MINVALUE 1 OWNED BY " + tableName + "." + primaryKey + ";", new object[0]);
        }

        public static void SetSequenceOnPkColumn(string tableName, string primaryKey, string sequenceName)
        {
            NewContext.Database.ExecuteSqlCommand("ALTER TABLE " + tableName + " ALTER COLUMN " + primaryKey + " set DEFAULT nextval('" + sequenceName + "');", new object[0]);
        }

        public static void SetSequenceForTable(string tableName, string primaryKey, string sequenceName)
        {
            //Update table sequence (result of query MUST be enumerated!)
            var maxId = NewContext.Database.SqlQuery<int>("SELECT MAX(" + primaryKey + ") FROM " + tableName + ";").FirstOrDefault();
            var startValue = NewContext.Database.SqlQuery<int>("SELECT setval('" + sequenceName + "','" + maxId + "', true);").FirstOrDefault();
        }

        public static void ResetSequenceForTable(string sequenceName)
        {
            //Reset table sequence (result of query MUST be enumerated!)
            NewContext.Database.ExecuteSqlCommand("ALTER SEQUENCE " + sequenceName + " RESTART WITH 1;", new object[0]);
        }
    }
}
