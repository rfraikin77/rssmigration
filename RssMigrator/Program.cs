﻿using RssMigrator.MigrationSteps;

namespace RssMigrator
{
    class Program
    {
        static void Main()
        {
            new Migrator().Start();
        }
    }
}
