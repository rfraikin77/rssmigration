//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RssMigrator.NewContext
{
    using System;
    using System.Collections.Generic;
    
    public partial class msflow_service_requests
    {
        public int id { get; set; }
        public string user_name { get; set; }
        public System.DateTime expiry_date { get; set; }
        public System.DateTime notification_date { get; set; }
        public string expiry_trigger_url { get; set; }
        public string notification_trigger_url { get; set; }
        public string action_success_trigger_url { get; set; }
        public string action_failure_trigger_url { get; set; }
    }
}
