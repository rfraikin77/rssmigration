//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RssMigrator.NewContext
{
    using System;
    using System.Collections.Generic;
    
    public partial class ter_error_report_file
    {
        public string path { get; set; }
        public int file_size { get; set; }
        public System.DateTime date_created { get; set; }
        public string computer_name { get; set; }
        public string username { get; set; }
        public Nullable<int> administration_id { get; set; }
        public string administration_name { get; set; }
        public string administration_path { get; set; }
        public int id { get; set; }
        public int customer_error_report_id { get; set; }
    }
}
