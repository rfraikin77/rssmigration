//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RssMigrator.NewContext
{
    using System;
    using System.Collections.Generic;
    
    public partial class tvc_release_note_notification_tmp
    {
        public Nullable<System.DateTime> date_created { get; set; }
        public Nullable<System.DateTime> date_notified { get; set; }
        public string email_notified { get; set; }
        public Nullable<int> disabled { get; set; }
        public Nullable<int> is_email_valid { get; set; }
        public Nullable<int> note_id { get; set; }
        public Nullable<int> customer_id { get; set; }
        public Nullable<int> user_id { get; set; }
        public Nullable<int> file_error_report_id { get; set; }
        public int id { get; set; }
    }
}
