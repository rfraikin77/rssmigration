//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RssMigrator.NewContext
{
    using System;
    using System.Collections.Generic;
    
    public partial class tp_contact_request
    {
        public string name { get; set; }
        public string company { get; set; }
        public string address { get; set; }
        public string postal_code { get; set; }
        public string city { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public Nullable<int> type_id { get; set; }
        public Nullable<int> category { get; set; }
        public string question { get; set; }
        public Nullable<int> website_id { get; set; }
        public System.DateTime audit_date { get; set; }
        public int id { get; set; }
    }
}
