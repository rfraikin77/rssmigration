//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RssMigrator.NewContext
{
    using System;
    using System.Collections.Generic;
    
    public partial class release_change_log
    {
        public string description { get; set; }
        public System.DateTime date_logged { get; set; }
        public int release_id { get; set; }
        public Nullable<int> changed_by { get; set; }
        public int id { get; set; }
    }
}
